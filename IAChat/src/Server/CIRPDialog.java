/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import NetPackage.INetPackage;
import NetPackage.NetDialog;
import NetPackage.NetPackage;
import Protocoles.CIRP.CIRPServer;
import java.io.IOException;
import java.net.Socket;

/**
 *
 * @author Fluxio
 */
public class CIRPDialog extends NetDialog {
    
    public CIRPDialog(Socket connection) throws IOException {
        super(connection);
        
    }
    
    @Override
    public void run() {
        Continue = true;
        INetPackage Package;
        
        while(Continue) {
            try {
                Package = new NetPackage();
                
                System.out.println("---");
                System.out.println("Waiting for a request...");
                
                //READING
                Package.readNetworkString(BR.readLine());
                System.out.println("Received package : " + Package.getNetworkString());
                
                
                //PROCESSING
                switch (((NetPackage)Package).getArg(0)) {
                    case "CONNECT":
                        Package = CIRPServer.Connect((NetPackage)Package);
                        break;
                    default:
                        System.out.println("Command not found...");
                        Package.clearArgs();
                }
                
                //WRITING
                DOS.write(Package.getNetworkBytes());
            } catch (IOException ex) {
                System.out.println(ex);
                Continue = false;
            }
        }
        
    }
}
