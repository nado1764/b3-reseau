#include <errno.h>
#include <sys/types.h>
#include <sys/socket.h>
#include "TCPClient.hpp"

namespace tcpc {

bool TCPClient::Connect() {
	int ret = connect(m_socketFD
					, (struct sockaddr*)&m_socketAddress
					, sizeof(m_socketAddress));

	if(ret == 0)
		return true;

	// TODO: could be a switch with each possible errno
	if(errno == ECONNREFUSED)
		fprintf(stderr, "[-] Server unavailable\n");
	else
		fprintf(stderr, "[-] Connection error : errno = %d\n", errno);

	return false;
}

}
