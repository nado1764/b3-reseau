/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NetPackage;

import static NetPackage.NetPackageConstants.PackageRowSep;
import static NetPackage.NetPackageConstants.PackageSep;
import java.io.UnsupportedEncodingException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

/**
 *
 * @author Fluxio
 */
public class NetTablePackage implements INetPackage {
    ArrayList<String>            LabelArgs;
    ArrayList<ArrayList<String>> ContentArgs;
    
    
    
    //LABEL
    public NetTablePackage() {
        LabelArgs = new ArrayList<>();
        ContentArgs = new ArrayList<>();
    }
    
    public void appendLabel(String Label) {
        LabelArgs.add(Label);
    }
    
    public String getLabel(int LabelPosition) {
        return LabelArgs.get(LabelPosition);
    }
    
    public void clearLabelArgs() {
        LabelArgs.clear();
    }
    
    public int getLabelCount() {
        return LabelArgs.size();
    }
    
    //CONTENT
    public void appendContentRow(ArrayList<String> ContentRow) {
        ContentArgs.add(ContentRow);
    }
    
    public ArrayList<String> getContentRow(int RowPosition) {
        return ContentArgs.get(RowPosition);
    }
    
    public void clearContentArgs() {
        ContentArgs.clear();
    }
    
    public int getContentRowCount() {
        return ContentArgs.size();
    }
    
    //
    @Override
    public void clearArgs() {
        clearLabelArgs();
        clearContentArgs();
    }
    
    //DB INTEGRATION
    public void readResultSet(ResultSet RS) {
        ArrayList<String> Row;
        ResultSetMetaData RSMD;
        
        try {
            RSMD = RS.getMetaData();
            
            //APPEND LABEL
            for(int i = 1; i <= RSMD.getColumnCount(); i++) {
                appendLabel(RSMD.getColumnLabel(i));
            }
            
            //APPEND CONTENT
            while(RS.next()) {
                Row = new ArrayList<>();
                
                for(int i = 1; i <= RSMD.getColumnCount(); i++)
                {
                    Row.add((RS.getObject(i) == null)? "" :RS.getObject(i).toString()); //Si on récupère "NULL" on renvoie une chaine vide
                }
                
                appendContentRow(Row);
            }
            
        } catch (SQLException ex) {
            System.out.println(ex);
        }
    }
    
    //NETWORK
    @Override
    public String getNetworkString(boolean appendCR) {
        String Message = "";
        
        for(String Label : LabelArgs) {
            Message += Label + PackageSep;
        }
        
        Message += PackageRowSep;
        
        for(ArrayList<String> Row : ContentArgs) {
            for(String ContentArg : Row) {
                Message += ContentArg + PackageSep;
            }
            
            Message += PackageRowSep;
        }
        
        return Message + (appendCR ? '\n' : "");
    }
    
    @Override
    public String getNetworkString() {
        return getNetworkString(true);
    }
    
    @Override
    public byte[] getNetworkBytes() {
        try {
            return getNetworkString().getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            System.err.println("Warning : cannot convert string to UTF-8 bytes");
            return null;
        }
    }
    
    @Override
    public void readNetworkString(String Message) {
        ArrayList<String> ReadTable = new ArrayList<>();
        ReadTable.addAll(new ArrayList<>(Arrays.asList(Message.split(PackageRowSep))));
        LabelArgs.addAll(new ArrayList<>(Arrays.asList((ReadTable.get(0)).split(PackageSep))));

        for(int i = 1; i < ReadTable.size(); i++) {
            ContentArgs.add(new ArrayList<>(Arrays.asList((ReadTable.get(i)).split(PackageSep))));
        }
    }
}
