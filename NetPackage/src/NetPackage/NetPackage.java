/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NetPackage;


import static NetPackage.NetPackageConstants.PackageSep;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Fluxio
 */
public class NetPackage implements INetPackage {
    private ArrayList<String> Args;
    
    public NetPackage() {
        Args = new ArrayList<>();
    }
    
    public NetPackage(String arg0) {
        Args = new ArrayList<>();
        Args.add(arg0);
    }
    
    public NetPackage(ArrayList<String> args) {
        Args = new ArrayList<>();
        
        for(String s : args) {
            Args.add(s);
        }
    }
    
    public NetPackage appendArg(String Arg) {
        Args.add(Arg);
        return this;
    }
    
    public String getArg(int ArgPosition) {
        return Args.get(ArgPosition);
    }
    
    public List<String> getList() {
        return (List<String>)Args.clone();
    }
    
    @Override
    public void clearArgs() {
        Args.clear();
    }
    
    @Override
    public String getNetworkString(boolean appendCR) {
        String Message = "";
        
        for(String Arg : Args) {
            Message += Arg + PackageSep;
        }
        
        return Message + (appendCR ? '\n' : "");
    }
    
    @Override
    public String getNetworkString() {
        return getNetworkString(true);
    }
    
    @Override
    public byte[] getNetworkBytes(){
        try {
            return getNetworkString().getBytes("UTF-8");
        } catch (Exception ex) {
            System.out.println("Warning : cannot convert string to UTF-8 bytes");
            return null;
        }
    }
    
    @Override
    public void readNetworkString(String Message) {
        Args.addAll(Arrays.asList(Message.split(PackageSep)));
    }
}
