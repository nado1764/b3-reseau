/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LUGAP;

import NetPackage.NetPackage;
import NetPackage.NetTablePackage;
import BeanDBAccess.BeanDBAccessMySql;
import java.security.MessageDigest;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Fluxio
 */
public class LUGAPServer {
    BeanDBAccessMySql accessBean;
    
    boolean isLoggedIn;
    
    public LUGAPServer() {
        try {
            isLoggedIn = false;
            
            //DB Access
            accessBean = new BeanDBAccessMySql();

            accessBean.Connect();
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println("Cannot reach DB : " + ex);
        } catch (Exception ex) {
            System.out.println("Error : " + ex);
        }
    }
    
    public NetTablePackage DisplayFlights(NetPackage Package) {
        NetTablePackage TPackage = new NetTablePackage();
        
        ResultSet RS;
        String Query = "SELECT * " +
                       "FROM vols;" ;

        try {
            RS = accessBean.ExecuteQuery(Query);
            TPackage.readResultSet(RS);
            
            return TPackage;
        } catch(Exception ex) {
            System.out.println(ex);
            TPackage.clearArgs();
        }
        
        return TPackage;
    }
    
    public NetTablePackage DisplayLuggages(NetPackage Package) {
        NetTablePackage TPackage = new NetTablePackage();
        ResultSet RS;
        String Query = "SELECT bagages.* " +
                       "FROM vols INNER JOIN billets " +
                       "ON vols.Id = billets.Vol " +
                       "INNER JOIN billetsbagages " +
                       "ON billets.Id = billetsbagages.IdBillet " +
                       "INNER JOIN bagages " +
                       "ON billetsbagages.IdBagage = bagages.Id " +
                       "WHERE vols.Id = " + Package.getArg(1) + ";" ;
        
        try {
            RS = accessBean.ExecuteQuery(Query);
            
            TPackage.readResultSet(RS);
            
            return TPackage;
        } catch(SQLException ex) {
            System.out.println(ex);
            TPackage.clearLabelArgs();
            TPackage.clearContentArgs();
        }
        
        return TPackage;
    }
    
    public NetPackage Connect(NetPackage Package) {
        MessageDigest messageDigest;
        byte[] messageDigestMD5;
        StringBuffer stringBuffer = new StringBuffer();
        String Query = "SELECT Password " + 
                       "FROM Agents " + 
                       "WHERE Nom = '" + Package.getArg(1) + "';";
        
        ResultSet RS;
        
        try {            
            RS = accessBean.ExecuteQuery(Query); //Password retreived
            RS.next();
            
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(Package.getArg(3).getBytes()); //ajout du salt
            messageDigest.update(RS.getString("Password").getBytes());
            messageDigestMD5 = messageDigest.digest();
            
            
            for(byte bytes : messageDigestMD5) {
                stringBuffer.append(String.format("%02x", bytes & 0xff));
            }
            
            if(Package.getArg(2).equals(stringBuffer.toString())){
                isLoggedIn = true;
                
                Package.clearArgs();
                Package.appendArg("TRUE");
            } else {
                Package.clearArgs();
                Package.appendArg("FALSE");
            }
            
            return Package;
            
        } catch (Exception e) {
            System.out.println(e);
        }
        
        Package.clearArgs();
        return Package;
    }
    
    public NetPackage Received(NetPackage Package) {
        String Query = "UPDATE bagages " + 
                       "SET Received = " + Package.getArg(2) + " " +
                       "WHERE Id = " + Package.getArg(1) + ";";
        
        try {
            Package.clearArgs();
            Package.appendArg((accessBean.ExecuteUpdate(Query) == 1) ? "TRUE" : "FALSE");
        } catch (Exception e) {
            System.out.println(e);
            Package.clearArgs();
        }
        
        return Package;
    }
    
    public NetPackage Charged(NetPackage Package) {
        String Query = "UPDATE bagages " + 
                       "SET Charged = " + Package.getArg(2) + " " +
                       "WHERE Id = " + Package.getArg(1) + ";";
        
        try {
            Package.clearArgs();
            Package.appendArg((accessBean.ExecuteUpdate(Query) == 1) ? "TRUE" : "FALSE");
        } catch (Exception e) {
            System.out.println(e);
            Package.clearArgs();
        }
        
        return Package;
    }
    
    public NetPackage Verified(NetPackage Package) {
        String Query = "UPDATE bagages " + 
                       "SET Verified = " + Package.getArg(2) + " " +
                       "WHERE Id = " + Package.getArg(1) + ";";
        
        try {
            Package.clearArgs();
            Package.appendArg((accessBean.ExecuteUpdate(Query) == 1) ? "TRUE" : "FALSE");
        } catch (Exception e) {
            System.out.println(e);
            
            Package.clearArgs();
        }
        
        return Package;
    }
    
    public NetPackage Comment(NetPackage Package) {
        String Query = "UPDATE bagages " + 
                       "SET Comment = '" + Package.getArg(2) + "' " +
                       "WHERE Id = " + Package.getArg(1) + ";";
        
        try {
            Package.clearArgs();
            Package.appendArg((accessBean.ExecuteUpdate(Query) == 1) ? "TRUE" : "FALSE");
        } catch (Exception e) {
            System.out.println(e);
            
            Package.clearArgs();
        }
        
        return Package;
    }
}
