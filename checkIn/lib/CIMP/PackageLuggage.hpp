#ifndef PACKAGELUGGAGE_H_INCLUDED
#define PACKAGELUGGAGE_H_INCLUDED

#include <string>

#include "Package.hpp"

namespace CIMP {
  class PackageLuggage : public Package {
    private:

    public:
      //CONSTRUCTORS
      PackageLuggage();
	  PackageLuggage(const PackageLuggage &p);
      PackageLuggage(float Poids, bool isValise);

      //METHODS
      std::tuple<std::string, std::string> getParams();
  };
}

#endif //PACKAGELUGGAGE_H_INCLUDED
