#include <string>
#include <tuple>
#include <vector>

#include "ConfParser/ConfParser.h"

#include "actions.hpp"

using namespace std;
using namespace CIMP;

bool checkLogin(std::tuple<std::string,std::string> credentials) {
	ConfParser cp("../data/credentials.csv", ';');

	return cp.checkTuple(credentials);
}

bool checkTicket(std::tuple<std::string,std::string> ticket) {
	ConfParser cp("../data/Tickets.csv", ';');

	return cp.checkTuple(ticket);
}

float checkLuggages(vector<PackageLuggage> Luggages) {
	float Excess = 0.0;

	for(auto L : Luggages) {
		string strPoids = get<0>(L.getParams());
		float fPoids;

		try {
			fPoids = stof(strPoids, nullptr);

			//TODO : penser à inclure le poids max dans le fichier conf
			if(fPoids > 20.0)
				Excess += (fPoids - 20);
		} catch (exception ex) {
			cerr << ex.what();
		}
	}

	return Excess;
}
