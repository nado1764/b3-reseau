#ifndef PACKAGE_H_INCLUDED
#define PACKAGE_H_INCLUDED

#include <string>
#include <vector>

#include "constants.hpp"

namespace CIMP {
  class Package {
    protected:
      //VARIABLES
      MsgType PackageType;
      char Separator = '#';
      std::vector<std::string> Args;

    public:
      //CONSTRUCTORS
      Package ();
      Package (const Package& P);

      //SETTERS
      void setType(MsgType PackageType);
      void setSep(char Separator);

      //GETTERS
      MsgType getType() const;
      char getSep() const;
      std::string getMsg() const;
      std::string getArg(int i);

      //METHODS
      static Package* Create(std::vector<std::string> readArgs, bool guess = true);

      //DESTRUCTOR
      ~Package();
  };
}

#endif //PACKAGE_H_INCLUDED
