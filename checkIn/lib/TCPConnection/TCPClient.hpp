#ifndef TCPCLIENT_HPP
#define TCPCLIENT_HPP

#include "TCPNegociation.hpp"
#include "TCPService.hpp"

namespace tcpc {

class TCPClient : public TCPNegociation, public TCPService {
	private:
		TCPClient();
	public:
		TCPClient(const std::string &host, const uint16_t port)
			: TCPNegociation::TCPNegociation(host, port) {};
		bool Connect();
};

}
#endif
