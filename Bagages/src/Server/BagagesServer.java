/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.Properties;
/**
 *
 * @author nado
 */
public class BagagesServer {
    Properties props;
    ServerSocket lugapSocket, lugapmSocket;
    
    // TODO: checkin ignoré ici
    BagagesServer() {
        loadProps();
        
        // Luggage server
        startLugServer();        
    }
    
    private void loadProps() {
        props = new Properties();

        try (FileInputStream file = new FileInputStream("server.properties")) {
            props.load(file);
        }
        catch(IOException e) {
            System.out.println(e.getMessage());
        }
    }
    
    private void startLugServer() {
        try {
            lugapSocket = new ServerSocket(Integer.parseInt(props.getProperty("PORT_BAGAGES", "7000")),
                                          Integer.parseInt(props.getProperty("MAX_CONNS", "5")));
            lugapmSocket = new ServerSocket(Integer.parseInt(props.getProperty("PORT_LUGAPM", "7002")),
                                            Integer.parseInt(props.getProperty("MAX_CONNS", "5")));
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public void listen() {
        Thread lugapT;
        lugapT = new Thread() {
            @Override
            public void run() {
                while(true) {
                    try {
                        System.out.println("[*][L] En attende de connexion");
                        Socket connection = lugapSocket.accept();
                        Thread t = new Thread(new LUGAPDialog(connection));
                        System.out.println("[+][L] Nouvelle connexion.");
                        t.start();
                    } catch (IOException ex) {
                        System.out.println(ex.getMessage());
                    }
                }
            }
        };
        
        Thread lugapmT = new Thread() {
            @Override
            public void run() {
                while(true) {
                    try {
                        System.out.println("[*][LM] En attende de connexion");
                        Socket connection = lugapmSocket.accept();
                        Thread t = new Thread(new LUGAPMDialog(connection));
                        System.out.println("[+][LM] Nouvelle connexion.");
                        t.start();
                    } catch (Exception ex) {
                        System.err.println(ex.getMessage());
                    }
                }
        }};
        
        lugapT.start();
        lugapmT.start();
        
        while(lugapT.isAlive() && lugapmT.isAlive()) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {}
        }
        
        if(lugapT.isAlive()) {
            lugapT.interrupt();
        }
        if(lugapmT.isAlive()) {
            lugapmT.interrupt();
        }
        
        try {
            System.out.println("[*] Fermeture des connexions");
            lugapSocket.close();
            lugapmSocket.close();
        } catch (IOException ex) {
            System.err.println("[-] Erreur à la fermeture des sockets");
        }
        
    }
    
    public static void main(String[] args) {
        BagagesServer Serv = new BagagesServer();
        Serv.listen();
    }
}
