/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TICKMAP;

import NetPackage.NetPackage;

/**
 *
 * @author Fluxio
 */
public class TICKMAPClient {
    NetPackage Package;
    
    //CONSTRUCTORS
    public TICKMAPClient() {
        Package = new NetPackage();
    }
    
    //DISPLAY PROTOCOL
    public NetPackage DisplayFlights() {
        Package.clearArgs();
        Package.appendArg("DISPLAYFLIGHTS");
        
        return Package;
    }
    
    public NetPackage Connect(String Login, String Password, String salt) {
        Package.clearArgs();
        Package.appendArg("CONNECT");
        Package.appendArg(Login);
        Package.appendArg(Password);
        Package.appendArg(salt);
        
        return Package;
    }
    
    public NetPackage InscriptionReservation(String Nom, String Prenom, String Mdp, String Naissance, String Sexe, String Nationalite, String Pseudo, String Flight) {
        Package.clearArgs();
        Package.appendArg("INSCRIPTION_RESERVATION");
        Package.appendArg(Nom);
        Package.appendArg(Prenom);
        Package.appendArg(Mdp);
        Package.appendArg(Naissance);
        Package.appendArg(Sexe);
        Package.appendArg(Nationalite);
        Package.appendArg(Pseudo);
        Package.appendArg(Flight);
        
        return Package;
    }
    
    public NetPackage ConnexionReservation(String UserName, String Password, String Flight) {
        Package.clearArgs();
        Package.appendArg("CONNEXION_RESERVATION");
        Package.appendArg(UserName);
        Package.appendArg(Password);
        Package.appendArg(Flight);
        
        return Package;
    }
    
    public NetPackage ConfirmerReservation(String Confirmer) { //TODO passer le HMAC en parametre
        Package.clearArgs();
        Package.appendArg("CONFIRMATION_RESERVATION");
        Package.appendArg(Confirmer);
        //Package.appendArg(HMAC);
        
        return Package;
    }
    
    public NetPackage PayerReservation(String NumCarteBanc, String Owner, String Price) {
        Package.clearArgs();
        Package.appendArg("PAIEMENT_RESERVATION");
        Package.appendArg(NumCarteBanc);
        Package.appendArg(Owner);
        Package.appendArg(Price);
        
        return Package;
    }
    
    public NetPackage PayForPassengerId(Integer id) {
        Package.clearArgs();
        
        Package.appendArg("PAYFORID");
        Package.appendArg(id.toString());
        
        return Package;
    }
}
