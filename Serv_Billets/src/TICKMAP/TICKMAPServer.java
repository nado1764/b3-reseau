/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TICKMAP;

import BeanDBAccess.BeanDBAccessMariadb;
import BeanDBAccess.BeanDBAccessMySql;
import NetPackage.INetPackage;
import NetPackage.NetPackage;
import NetPackage.NetTablePackage;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Fluxio
 */
public class TICKMAPServer {
    BeanDBAccessMariadb accessBean;
    
    boolean isLoggedIn;
    
    public TICKMAPServer() throws IOException, SQLException, ClassNotFoundException {
        isLoggedIn = false;

        //DB Access
        accessBean = new BeanDBAccessMariadb();

        accessBean.Connect();
    }
    
    public NetTablePackage DisplayFlights(NetPackage Package) {
        NetTablePackage TPackage = new NetTablePackage();
        
        ResultSet RS;
        String Query = "SELECT * " +
                       "FROM vols " +
                       "WHERE Depart >= SYSDATE();";
                        //+ "AND Depart <= DATE_ADD(SYSDATE(), INTERVAL 7 DAY);" ;

        try {
            RS = accessBean.ExecuteQuery(Query);
            TPackage.readResultSet(RS);
            
            return TPackage;
        } catch(SQLException ex) {
            System.out.println(ex);
            TPackage.clearArgs();
        }
        
        return TPackage;
    }
    
    public NetPackage Connect(NetPackage Package) {
        MessageDigest messageDigest;
        byte[] messageDigestMD5;
        StringBuffer stringBuffer = new StringBuffer();
        String Query = "SELECT Password " + 
                       "FROM agents " + 
                       "WHERE Nom = '" + Package.getArg(1) + "';";
        
        ResultSet RS;
        
        try {            
            RS = accessBean.ExecuteQuery(Query); //Password retreived
            RS.next();
            
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(Package.getArg(3).getBytes()); //ajout du salt
            messageDigest.update(RS.getString("Password").getBytes());
            messageDigestMD5 = messageDigest.digest();
            
            
            for(byte bytes : messageDigestMD5) {
                stringBuffer.append(String.format("%02x", bytes & 0xff));
            }
            
            if(Package.getArg(2).equals(stringBuffer.toString())){
                isLoggedIn = true;
                
                Package.clearArgs();
                Package.appendArg("TRUE");
            } else {
                Package.clearArgs();
                Package.appendArg("FALSE");
            }
            
            return Package;
            
        } catch (NoSuchAlgorithmException | SQLException e) {
            System.out.println(e);
        }
        
        Package.clearArgs();
        return Package;
    }
    
    public NetPackage ConnexionReservation(NetPackage Package) throws SQLException { // CALLED BY SER_BILLETS
        //On reçoit un Pseudo + MDP + Numero de vol
        ResultSet RS;
        String Query = 
                "SELECT password " +
                "FROM passagers " +
                "WHERE username = '" + Package.getArg(1) + "';";

        RS = accessBean.ExecuteQuery(Query);
        //check Pseudo & MDP (Passager)
        if(!RS.next()) {
            //SI NON
            //Renvoyer Msg erreur
            Package.clearArgs();
            Package.appendArg("ERROR_WRG_PSWD");
            System.out.println("Package send : " + Package.getNetworkString());
            
            return Package;
        }
        
        //phase de récupération des sièges
        Query = "select tot.cnt - res.cnt as PlacesDispo " +
                "from " +
                "(select billets.vol, count(billets.id) as cnt " +
                "from billets " +
                "where billets.vol = " + Package.getArg(3) + ") as res, " +
                "(select vols.id, vols.places as cnt " +
                "from vols " +
                "where vols.id = " + Package.getArg(3) + ") as tot;";
        
        RS = accessBean.ExecuteQuery(Query);
        RS.next();
        int NbPlacesDispo = RS.getInt("PlacesDispo");
        
        
        //Vérifier la disponibilité des sieges et en choisir un ou plusieurs à attribuer au(x) passager(s)
        if(NbPlacesDispo <= 0) { //par defaut, il y a toujours des places... pour le moment
            //SI NON
            //Renvoyer Msg erreur
            Package.clearArgs();
            Package.appendArg("ERROR_SEATS_EMPTY");
            System.out.println("Package send : " + Package.getNetworkString());
            
            return Package;
        
        }
        
        Query = "SELECT prix " +
                "FROM vols " + 
                "WHERE id = " + Package.getArg(3) + ";";
        
        RS = accessBean.ExecuteQuery(Query);
        RS.next();
        double Prix = RS.getDouble("Prix");
        
        //Renvoyer les sièges choisis aux clients + Prix à payer
        Package.clearArgs();
        Package.appendArg("SIEGE_MONTANT_RESERVATION");
        Package.appendArg(NbPlacesDispo + ""); //Changer la place attribuée, pas forcément la dernière?
        Package.appendArg(Prix + "");
        System.out.println("Package send : " + Package.getNetworkString());
        
        return Package;
    }
    
    public NetPackage InscriptionReservation(NetPackage Package) throws SQLException {
        //On reçoit Nom + Prenom + MDP + Date de naissance + Sexe + Nationalite + Pseudo
        ResultSet RS;
        String Query = 
                "SELECT id " +
                "FROM pays " + 
                "WHERE UPPER(Nom) = UPPER('" + Package.getArg(6) + "');";
        
        RS = accessBean.ExecuteQuery(Query);
        
        if(!RS.next()) {
            return new NetPackage("NATIONALITE_NOT_FOUND");
        }
        
        String Nationalite = RS.getInt("id") + "";
        Query = "INSERT INTO passagers (Nom, Prenom, Password, Naissance, Sexe, Nationalite, Username)  " +
                "VALUES('" + Package.getArg(1) + "', '" + Package.getArg(2) + "', '" + Package.getArg(3) + "', '" + Package.getArg(4) + "', '" + Package.getArg(5) + "', '" + Nationalite + "','" + Package.getArg(7) + "');";
        
        try {
            if(1 != accessBean.ExecuteUpdate(Query)){
                Package.clearArgs();
                Package.appendArg("ERROR_INSCRIPTION");
                
                return Package;
            }
        } catch (Exception ex) {
            System.err.println(ex);
            Package.clearArgs();
            Package.appendArg("ERROR_INSCRIPTION");
            Package.appendArg(ex.toString());
            
            return Package;
        }
        
        //phase de récupération des sièges
        Query = "select tot.cnt - res.cnt as PlacesDispo " +
                "from " +
                "(select billets.vol, count(billets.id) as cnt " +
                "from billets " +
                "where billets.vol = " + Package.getArg(8) + ") as res, " +
                "(select vols.id, vols.places as cnt " +
                "from vols " +
                "where vols.id = " + Package.getArg(8) + ") as tot;";
        
        RS = accessBean.ExecuteQuery(Query);
        RS.next();
        int NbPlacesDispo = RS.getInt("PlacesDispo");
        
        
        //Vérifier la disponibilité des sieges et en choisir un ou plusieurs à attribuer au(x) passager(s)
        if(NbPlacesDispo <= 0) { //par defaut, il y a toujours des places... pour le moment
            //SI NON
            //Renvoyer Msg erreur
            Package.clearArgs();
            Package.appendArg("ERROR_SEATS_EMPTY");
            System.out.println("Package send : " + Package.getNetworkString());
            
            return Package;
        
        }
        
        Query = "SELECT prix " +
                "FROM vols " + 
                "WHERE id = " + Package.getArg(8) + ";";
        
        RS = accessBean.ExecuteQuery(Query);
        RS.next();
        double Prix = RS.getDouble("Prix");
        
        //Renvoyer les sièges choisis aux clients + Prix à payer
        Package.clearArgs();
        Package.appendArg("SIEGE_MONTANT_RESERVATION");
        Package.appendArg(NbPlacesDispo + ""); //Changer la place attribuée, pas forcément la dernière?
        Package.appendArg(Prix + "");
        System.out.println("Package send : " + Package.getNetworkString());

        return Package;
    }
    
    public NetPackage ConfirmerReservation(NetPackage Package) {
        if(Package.getArg(1).equals("FALSE")) {
            Package.clearArgs();
            return Package;
        }
        
        Package.clearArgs();
        Package.appendArg("ACK_CONFIRMATION");
        
        return Package;
    }
    
    public NetPackage ConfirmerPaiement(boolean success, String name, String amount) {
        try {
            // column transactionDate default to sysdate()
            int ret = accessBean.ExecuteUpdate("insert into "
                    + "transactionslog (comment, success) "
                    + "values('" + name + ", " + amount + "', " + success
                    + ");");
            if(1 == ret)
            {
                return new NetPackage("PAIEMENT_CONFIRMED");
            }
        } catch (SQLException ex) {
            System.err.println("[-] SQL Exception: " + ex.getMessage());
        }
        return new NetPackage("SQLFAIL");
    }

    public INetPackage PayForId(String id) {
        NetPackage Package = new NetPackage("PAYFORID");
        try {
            
            
            int ret = accessBean.ExecuteUpdate("update billets set status='PAID' where passager = " + id +";");
            accessBean.ExecuteUpdate("insert into transactionslog (comment, success) "
                    + "values('Billets payés pour " + id +  "',"
                    + (ret >= 1) + ");");
            // we dont care about saying it succeeded or not, the caddie app checks it itself
            Package.appendArg("DONE");
        } catch (SQLException ex) {
            System.err.println("[-] SQL Exception: " + ex.getMessage());
            Package.clearArgs();
            Package.appendArg("SQLFAIL");
        }
        return Package;
    }
}
