#ifndef STRING_H_INCLUDED
#define STRING_H_INCLUDED

#include <string>
#include <sstream>
#include <vector>
#include <iterator>

namespace String {
    template<typename Out>
    void split(const std::string &s, char delim, Out result);
    std::vector<std::string> split(const std::string &s, char delim);
}

#endif // STRING_H_INCLUDED
