#include "constants.hpp"
#include "PackageLuggage.hpp"

#include <tuple>

using namespace std;

namespace CIMP {
  //CONSTRUCTORS
  PackageLuggage::PackageLuggage() {
    setType(MsgType::LUGGAGE);
  }

  PackageLuggage::PackageLuggage(const PackageLuggage &p) : PackageLuggage() {
	  this->Args = p.Args;
  }

  PackageLuggage::PackageLuggage(float Poids, bool isValise) {
    setType(MsgType::LUGGAGE);

    Args.clear();
  	Args.push_back(to_string(Poids));
  	Args.push_back(isValise ? "Y" : "N");
  }

  //METHODS
  tuple<string, string> PackageLuggage::getParams() {
    return make_tuple(Args[0], Args[1]);
  }
}
