/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caddie;

import BeanDBAccess.BeanDBAccessMariadb;
import java.io.IOException;
import java.net.URLEncoder;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nado
 */
@WebServlet(name = "Login", urlPatterns = {"/Login"})
public class Login extends HttpServlet {
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        if(request.getParameter("username") == null || request.getParameter("password") == null
            ||request.getParameter("username").equals("") || request.getParameter("password").equals("")) {
            response.sendRedirect("index.jsp?error=" + URLEncoder.encode("Le nom d'utilisateur et le mot de passe ne peuvent être vides", "utf8"));
            return;
        }
        BeanDBAccessMariadb AccessBean;
        try {
            AccessBean = DBWrapper.getCtx();
        } catch (SQLException ex) {
            response.sendRedirect("index.jsp?error=" + URLEncoder.encode("La connexion vers la base de données a échoué : " + ex.getMessage(), "utf8"));
            return;
        } catch(ClassNotFoundException ex) {
            response.sendRedirect("index.jsp?error=" + URLEncoder.encode("Class not found : " + ex.getMessage(), "utf8"));
            return;
        }
        
        
        String login = request.getParameter("username");
        String pwd = request.getParameter("password");
        
        HttpSession session = request.getSession();
        
        try {
            if(request.getParameter("new") != null) {
                ResultSet rs = AccessBean.ExecuteQuery("select id from passagers where username ='" + login + "';");
                if(rs.next()) {
                    response.sendRedirect("index.jsp?error=" + URLEncoder.encode("Un utilisateur existe déjà pour ce login", "utf8"));
                    return;
                }
                
                if(1 == AccessBean.ExecuteUpdate("insert into passagers(username, password) values('" + login + "', '" + pwd + "');")) {
                    rs = AccessBean.ExecuteQuery("select id from passagers where username = '" + login + "';");
                    rs.next();
                    setAttrs(session, login, rs.getInt("id"));
                    Caddie.getCaddieNb(session, AccessBean, rs.getInt("id"));
                    response.sendRedirect("JSPCaddie.jsp");
                    return;
                }
                response.sendRedirect("index.jsp?error=" + URLEncoder.encode("Fail", "utf8"));
            }
            else {
                ResultSet rs = AccessBean.ExecuteQuery("select id from passagers where username = '" + login + "' "
                                                     + "and password = '" + pwd + "';");


                if(!rs.next()) {
                    response.sendRedirect("index.jsp?error=" + URLEncoder.encode("Login ou mot de passe incorrect", "utf8"));
                    return;
                }
                setAttrs(session, login, rs.getInt("id"));
                Caddie.getCaddieNb(session, AccessBean, rs.getInt("id"));
                response.sendRedirect("JSPCaddie.jsp");

            }
        } catch (SQLException ex) {
            response.sendRedirect("index.jsp?error=" + URLEncoder.encode("Une exception SQL est survenue : " + ex.getMessage(), "utf8"));
        }
    }
    
    private void setAttrs(HttpSession session, String login, int id) {
        session.setAttribute("username", login);
        session.setAttribute("id", id);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        if(request.getParameter("logout") != null && request.getParameter("logout").equals("true")) {
            HttpSession session = request.getSession();
            session.removeAttribute("username");
            session.removeAttribute("id");
            session.removeAttribute("caddie");
        }
        response.sendRedirect("index.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Login";
    }// </editor-fold>
}
