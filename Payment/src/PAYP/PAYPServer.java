/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PAYP;

import NetPackage.NetPackage;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.Properties;
import javax.net.ssl.SSLSocketFactory;
import sebatrap.client.ClientAccount;

/**
 *
 * @author Fluxio
 */
public class PAYPServer {
    DataOutputStream DOS;
    BufferedReader BR;
    Socket socket;
    
    // connect to serveur_mastercard over ssl
    private void getSSLConnection() throws IOException {
        Properties props = new Properties();

        FileInputStream file;
        try {
            file = new FileInputStream("server.properties");
            props.load(file);
        } catch (IOException ex) {
            System.err.println("[-] File not found for paypserver properties: " + ex.getMessage());
        }
        
        
        SSLSocketFactory sslSocketFactory = 
                (SSLSocketFactory)SSLSocketFactory.getDefault();
        
        String host = props.getProperty("HOST_BANK", "localhost");
        int port = Integer.parseInt(props.getProperty("PORT_BANK", "7006"));
        
        socket = sslSocketFactory.createSocket(host, port);
        
        DOS = new DataOutputStream(socket.getOutputStream());
        BR = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }
    
    
    public NetPackage Pay(String cardNumber, String name, float amount) {
        NetPackage pkg = new NetPackage();
        ClientAccount account;
        
        if(amount <= 0.0 && name.length() <= 1) {
            System.out.println("[-] Incorrect amount or name");
            return new NetPackage("FAIL");
        }
        
        try {
            account = new ClientAccount(cardNumber);
        } catch (Exception ex) {
            System.out.println("[-] Incorrect card number");
            return new NetPackage("FAIL");
        }

        try {
            getSSLConnection();
            
            pkg = account.verifPackage(amount);
            DOS.write(pkg.getNetworkBytes());
            pkg.clearArgs();
            pkg.readNetworkString(BR.readLine());
            if(pkg.getArg(0).equals("FAIL"))
                return new NetPackage("FAIL");
            
            pkg = account.payPackage();
            DOS.write(pkg.getNetworkBytes());
            pkg.clearArgs();
            pkg.readNetworkString(BR.readLine());
            socket.close();
        } catch (IOException ex) {
            System.err.println("[-] Could not execute payment: " + ex.getMessage());
        }
        if(pkg.getArg(0).equals("OK"))
            return new NetPackage("OK");
        return new NetPackage("FAIL");
    }
}
