#include "ConfParser.h"
#include "String/String.h"

#include <fstream>
#include <algorithm>
#include <tuple>

using namespace std;
using namespace String;

//CONSTRUCTORS
ConfParser::ConfParser(){}
ConfParser::ConfParser(const ConfParser& CP){
    Properties = CP.Properties;
}

ConfParser::ConfParser(string filename) : FileName(filename) {
	this->readProps();
}

ConfParser::ConfParser(string filename, char sep) : FileName(filename), Separator(sep) {
	this->readProps();
}

//GETTERS
unordered_map<string, string>& ConfParser::getProps() {
    return Properties;
}

string ConfParser::getFileName() {
    return FileName;
}

char ConfParser::getSep() {
    return Separator;
}

//SETTERS
void ConfParser::setProps(unordered_map<string, string> UM) {
    this->Properties = UM;
}

void ConfParser::setFileName(string FileName) {
    this->FileName = FileName;
}

void ConfParser::setSep(char Separator) {
    this->Separator = Separator;
}

//METHODS
bool ConfParser::AddProp(string Key, string Value) {
    auto search = getProps().find(Key);
    if(search != getProps().end())
        return false;
    else {
        getProps().insert(make_pair(Key, Value));
        return true;
    }
}

bool ConfParser::RemoveProp(string Key) {
    return getProps().erase(Key);
}

void ConfParser::DisplayProps() {
    for(auto Prop : getProps()) {
        cout << Prop.first << ' ' << getSep() << ' ' << Prop.second << endl;
    }
}

void ConfParser::writeProps() {
    ofstream ConfFile(getFileName());

    if(ConfFile.is_open()) {
        ConfFile << *this;
        ConfFile.close();
    } else {
        cout << "Unable to open file " << getFileName() << endl;
    }
}

bool ConfParser::readProps() {
    string line;
    ifstream ConfFile(getFileName());

    if(!ConfFile.is_open()) {
		cout << "Unable to open file " << getFileName() << endl;
		return false;
	}

	vector<string> subStrings;

	getProps().clear();

	while(getline(ConfFile, line)) {
		//On remove les espaces puis on s�pare
		line.erase(remove_if(line.begin(), line.end(), ::isspace), line.end());
		subStrings = split(line, getSep());

		if(!AddProp(subStrings[0], subStrings[1])) {
			cout << "Warning - Duplicates detected in read file : " << getFileName() << endl;
		}
	}

	ConfFile.close();
	return true;
}

bool ConfParser::checkTuple(tuple<string,string> t) {
	unordered_map<string,string>::const_iterator got = getProps().find(get<0>(t));

	if(got != getProps().end() && got->second == get<1>(t))
		return true;

	return false;
}

//OPERATORS
//istream& operator>>(istream &fluxLeft, ConfParser& ParserRight) {
//}

ostream& operator<<(ostream &fluxLeft, ConfParser& ParserRight) {
    for(auto Prop : ParserRight.getProps()) {
        fluxLeft << Prop.first << " " << ParserRight.Separator << " " << Prop.second << endl;
    }

    return fluxLeft;
}

string& ConfParser::operator[](string Arg) {
    return getProps()[Arg];
}

//DESTRUCTOR
ConfParser::~ConfParser() {}
