#include <atomic>
#include <chrono>
#include <iostream>
#include <memory>
#include <thread>
#include <vector>

#include "TCPConnection/TCPServer.hpp"
#include "TCPConnection/TCPService.hpp"

#include "CIMP/Session.hpp"
#include "CIMP/packages.hpp"

#include "actions.hpp"
#include "cimp_dialog.hpp"

extern volatile std::atomic<bool> sigCaught;

void cimp_listen(tcpc::TCPServer *server) {
	using namespace tcpc;
	using namespace std;

	vector<weak_ptr<TCPService>> sessions;

	std::cout << "[*] Waiting for a client to connect" << std::endl;
	while(!sigCaught) {
		shared_ptr<TCPService> connection(nullptr);
		try {
			connection = shared_ptr<TCPService>(server->Accept());
		}
		catch(std::string str) {
			std::cerr << "[-] Error accepting connections : " << str << std::endl;
			sigCaught = true;
			break;
		}

		if(connection == nullptr) {
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
			continue;
		}

		std::cout << "[+] Client connected" << std::endl;

		// add service socket to array
		sessions.push_back(connection);
		// start a thread for an cimpsession
		std::thread *t = new std::thread(cimp_session, connection);
	}
}

void cimp_session(std::shared_ptr<tcpc::TCPService> connection) {
	using namespace std;
	using namespace CIMP;

	Session session(connection);

	while(!sigCaught && session.isActive()) {
		Package *pkg = nullptr;

		// no package received, non blocking mode
		pkg = session.getMessage();
		if(pkg == nullptr) {
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
			continue;
		}


		if(!session.isNextType(pkg->getType())) {
			cout << "[-] Wrong type of package received : " << MsgTypetoString(pkg->getType()) << endl;
			session.sendAnswer(MsgType::ERROR, false, "Wrong msg type. (sequential error).");
			delete pkg;
			continue;
		}

		//ATTENTE D'UNE REQUETE LOGIN
		if(pkg->getType() == MsgType::LOGIN) {
			PackageLogin *p = static_cast<PackageLogin*>(pkg);
			if(checkLogin(p->getParams())) {
				// login granted
				cout << "[+] Client logged in" << endl;
				session.isLoggedIn(true);
				session.sendAnswer(MsgType::LOGIN);
			}
			else {
				cout << "[-] Client login denied" << endl;
				session.sendAnswer(MsgType::LOGIN, false, "Denied");
			}

			session.setLastType(MsgType::LOGIN);
		} //SINON - ON CHECK SI LOGGED IN
		else if (session.isLoggedIn()) {
			switch(pkg->getType()) {
				case MsgType::LOGOUT:
					// we dont care about the package here
					session.isLoggedIn(false);
					session.setLastType(MsgType::LOGOUT); //useless lol
					session.die();
					break;

				case MsgType::CHECK_TICKET:
					{
						PackageCheckTicket *p = static_cast<PackageCheckTicket*>(pkg);
						if(checkTicket(p->getParams())) {
							session.sendAnswer(MsgType::CHECK_TICKET);
						}
						else {
							session.sendAnswer(MsgType::CHECK_TICKET, false, "Denied");
						}

						session.setLastType(MsgType::CHECK_TICKET);
					}
					break;

				case MsgType::LUGGAGE:
					{
						PackageLuggage *p = static_cast<PackageLuggage*>(pkg);

						//SI PREMIER LUGGAGE, on vide la liste
						if(session.getLastType() == MsgType::CHECK_TICKET)
							session.Luggages.clear();

						//INSERTION LUGGAGE
						session.Luggages.push_back(*p);
						session.setLastType(MsgType::LUGGAGE);
					}
					break;

				case MsgType::CHECK_LUGGAGE:
					{
						cout << "[*] Check asked" << endl;
						float PoidsEx = checkLuggages(session.Luggages);
						cout << "[*] Overweight : " << PoidsEx << endl;

						if(session.sendAnswer(MsgType::CHECK_LUGGAGE, true, std::to_string(PoidsEx)))
							cout << "sent ok"<<endl;
						else
							cout << "sent not ok"<<endl;
					}
					break;

				case MsgType::PAYMENT_DONE:
					{
						//PackagePaymentDone *p = static_cast<PackagePaymentDone*>(pkg);
					}
					break;

				default:
					std::cerr << "[-] Paquet malformé reçu" << std::endl;
			} // switch(pkg->getType())
		} else { //SI PAS loggé - On renvoit un msg approprié
				session.sendAnswer(MsgType::ERROR, false, "Not logged in.");
		}

		delete pkg;
	} // while(!sigCaught && session.isActive())
}
