#ifndef PACKAGEPAYEMENTDONE
#define PACKAGEPAYEMENTDONE

#include "Package.hpp"

namespace CIMP {
  class PackagePaymentDone : public Package {
    public:
      PackagePaymentDone();
  };

}

#endif //PACKAGEPAYEMENTDONE
