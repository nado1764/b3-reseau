#ifndef ACTIONS_HPP
#define ACTIONS_HPP

#include "CIMP/PackageLuggage.hpp"

bool checkLogin(std::tuple<std::string,std::string> credentials);
bool checkTicket(std::tuple<std::string,std::string> ticket);
float checkLuggages(std::vector<CIMP::PackageLuggage> Luggages);

#endif /* ACTIONS_HPP */
