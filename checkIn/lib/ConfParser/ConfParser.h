#ifndef CONFPARSER_H_INCLUDED
#define CONFPARSER_H_INCLUDED

#include <iostream>
#include <unordered_map>
#include <string>
#include <tuple>

class ConfParser {
private :
    std::unordered_map<std::string, std::string> Properties;
    std::string FileName;
    char Separator = '=';

    //GETTERS

    //SETTERS
    void setProps(std::unordered_map<std::string, std::string> UM);

    //OPERATORS
    //friend std::istream& operator>>(std::istream &fluxLeft, ConfParser& ParserRight);
    friend std::ostream& operator<<(std::ostream &fluxLeft, ConfParser& ParserRight);

public :


    //CONSTRUCTORS
    ConfParser();
    ConfParser(const ConfParser& CP);
	ConfParser(std::string filename);
	ConfParser(std::string filename, char sep);

    //GETTERS
    std::unordered_map<std::string, std::string>& getProps();
    std::string getFileName();
    char getSep();

    //SETTERS
    void setFileName(std::string FileName);
    void setSep(char Separator);


    //METHODS
    bool AddProp(std::string Key, std::string Value);
    bool RemoveProp(std::string Key);
    void DisplayProps();
	bool checkTuple(std::tuple<std::string,std::string> t);

    void writeProps();
    bool readProps();

    //OPERATORS
    std::string& operator[](std::string Arg);

    //DESTRUCTOR
    ~ConfParser();
};

#endif // CONFPARSER_H_INCLUDED
