/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LUGAP;

import BeanDBAccess.BeanDBAccessMariadb;
import NetPackage.NetPackage;
import NetPackage.NetTablePackage;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author nado
 */
public class LUGAPMServer {
    
    BeanDBAccessMariadb accessBean;
    
    boolean isLoggedIn;
    
    public LUGAPMServer() throws IOException, SQLException, ClassNotFoundException {
        isLoggedIn = false;

        //DB Access
        accessBean = new BeanDBAccessMariadb("localhost", "3306", "default", "default", "airport");

        accessBean.Connect();
    }
    
    public NetPackage Connect(String login, String password) throws SQLException {
        String Query = "SELECT Password " + 
                       "FROM agents " + 
                       "WHERE upper(Nom) = upper('" + login + "')" +
                        " AND Password = '" + password + "'";
        
        NetPackage answer = new NetPackage("LOGIN");
        ResultSet RS;
        
        System.out.println("[LM][*] Authenticating " + login);
        RS = accessBean.ExecuteQuery(Query); //Password retreived

        if(RS.next()) {
            System.out.println("[LM][+] Success");
            answer.appendArg("OK");
        }
        else {
            System.out.println("[LM][-] Failure");
            answer.appendArg("FAIL");
        }
        return answer;
    }
    
    public NetPackage Connect(NetPackage p) throws SQLException {
        return Connect(p.getArg(1), p.getArg(2));
    }

    public NetTablePackage DisplayLuggages(NetPackage lugapPackage) {
        NetTablePackage TPackage = new NetTablePackage();
        ResultSet RS;
        String Query = "SELECT b.id, b.poids, b.type, billets.id " +
                       "FROM vols INNER JOIN billets " +
                       "ON vols.Id = billets.Vol " +
                       "INNER JOIN billetsbagages " +
                       "ON billets.Id = billetsbagages.IdBillet " +
                       "INNER JOIN bagages b " +
                       "ON billetsbagages.IdBagage = b.Id " +
                       "WHERE vols.Id = " + lugapPackage.getArg(1) +
                        " AND b.charged = 0;" ;
        
        try {
            RS = accessBean.ExecuteQuery(Query);
            
            TPackage.readResultSet(RS);
            
            return TPackage;
        } catch(SQLException ex) {
            System.out.println(ex);
            TPackage.clearLabelArgs();
            TPackage.clearContentArgs();
        }
        
        return TPackage;
    }
    
    
    public NetPackage SetCompleted(NetPackage lugapPackage) {
        List<String> ids = lugapPackage.getList();
        ids.remove(0);
        System.out.println(ids);
        String Query = "UPDATE bagages " + 
                       "SET Charged = 1 " +
                       "WHERE Id in (" + ids.toString().substring(1, ids.toString().length()-1) + ");";
        
        NetPackage answer = new NetPackage();
        answer.appendArg("CHARGED");
        try {
            answer.appendArg((accessBean.ExecuteUpdate(Query) == 1) ? "TRUE" : "FALSE");
        } catch (SQLException e) {
            System.out.println(e);
            answer.appendArg("ERROR");
        }
        
        return answer;
    }
}
