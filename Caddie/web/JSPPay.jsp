<%-- 
    Document   : JSPCaddie
    Created on : Dec 1, 2017, 9:10:32 PM
    Author     : nado
--%>
<%@page import="java.util.List"%>
<%@page import="caddie.Billets"%>
<%@page import="caddie.Billet"%>
<%@include file="WEB-INF/jspf/loginCheck.jspf"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="static/head.html"%>
        <title>JSP Pay</title>
    </head>
    <body>
        <%@include file="static/header.jsp"%>

        <div class="container">  
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Destination</th>
                            <th>Départ</th>
                            <th>Arrivée</th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                        List<Billet> l = Billets.getBilletsReserves((int)session.getAttribute("id"));
                        if(l != null) {
                        for(Billet b : l) {
                        %>
                        <tr>
                            <td><% out.print(b.id); %></td>
                            <td><% out.print(b.dest); %></td>
                            <td><% out.print(b.depart.toLocalDate()); %></td>
                            <td><% out.print(b.arrivee.toLocalDate()); %></td>
                        </tr>
                        <%
                        }}
                        %>
                    </tbody>
                </table>
            </div>
        </div>
         <div class="container">
            <div class="row" id="loginrow">
                <div class="col-md-4 center">
                    <div class="panel panel-default">
                        <div class="panel-heading centertext">
                            <h3 class="panel-title">Payez vos tickets ?</h3>
                        </div>
                        <div class="panel-body">
                            <form accept-charset="UTF-8" role="form" action="Caddie" method="POST">
                                <fieldset>
                                    <input class="btn btn-lg btn-success btn-block" type="submit" name="pay" value="Payer">
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<%@include file="static/footer.html" %>