#ifndef PACKAGES_HPP
#define PACKAGES_HPP

#include "constants.hpp"
#include "Package.hpp"
#include "PackageLogin.hpp"
#include "PackageLogout.hpp"
#include "PackageCheckTicket.hpp"
#include "PackageCheckLuggage.hpp"
#include "PackagePaymentDone.hpp"
#include "PackageLuggage.hpp"

#endif /* PACKAGES_HPP */
