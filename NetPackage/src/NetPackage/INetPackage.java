/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NetPackage;

/**
 *
 * @author Fluxio
 */
public interface INetPackage {
    void clearArgs();
    String getNetworkString(boolean appendCR); //FALSE en cas d'envoie crypté
    String getNetworkString();
    byte[] getNetworkBytes();
    void readNetworkString(String Message);
}
