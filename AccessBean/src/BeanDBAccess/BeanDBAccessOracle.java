/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BeanDBAccess;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Fluxio
 */
public class BeanDBAccessOracle extends BeanDBAccess{
    /******************************/
    /* STATIC FINAL               */
    /******************************/
    protected static final String DriverPropertiesFileName = "DBDriverProperties.properties";
    
    /******************************/
    /* VARIABLES                  */
    /******************************/
    public ResultSet Rs;
    
    /******************************/
    /* CONSTRUCTOR                */
    /**
     * @throws java.io.FileNotFoundException****************************/
    public BeanDBAccessOracle() throws IOException {
        super();
        
        this.Driver = "oracle.jdbc.driver.OracleDriver";
    }
    
    public BeanDBAccessOracle(String Host, String Port, String Login, String Password, String Schema) {
        super(Host, Port, Login, Password, Schema);
        this.Driver = "oracle.jdbc.driver.OracleDriver";
    }

    @Override
    public void Connect() throws SQLException, ClassNotFoundException {
        String Url = "jdbc:oracle:thin:@//" + getHost() + ":" + getPort() + "/orcl";
        
        Class.forName(getDriver());

        setConnection((Connection) DriverManager.getConnection(Url, getLogin(), getPassword()));

        setStatement((Statement) Connection.createStatement());
        
    }
}
