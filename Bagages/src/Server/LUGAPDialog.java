/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import NetPackage.NetPackage;
import LUGAP.LUGAPServer;
import java.io.IOException;
import java.net.Socket;
import NetPackage.INetPackage;
import NetPackage.NetDialog;

/**
 *
 * @author nado
 */
public class LUGAPDialog extends NetDialog {
    LUGAPServer lugap;

    public LUGAPDialog(Socket connection) throws IOException {
        super(connection);
        lugap = new LUGAPServer();

    }

    @Override
    public void run() {
        Continue = true;
        INetPackage Package;
        
        while(Continue) { //TODO : implementer un cas de sortie de boucle : timeout/disconnect
            try {
                Package = new NetPackage();
                
                Package.clearArgs();
                System.out.println("---");
                System.out.println("Waiting for a request...");
                
				
                Package.readNetworkString(BR.readLine());
                System.out.println("Received package : " + Package.getNetworkString());

                switch(((NetPackage)Package).getArg(0)) {
                    case "DISPLAYFLIGHTS" :
                        Package = lugap.DisplayFlights((NetPackage)Package);
                        break;
                    case "DISPLAYLUGGAGES" :
                        Package = lugap.DisplayLuggages((NetPackage)Package);
                        break;
                    case "CONNECT" :
                        Package = lugap.Connect((NetPackage)Package);
                        break;
                    case "RECEIVED" :
                        Package = lugap.Received((NetPackage)Package);
                        break;
                    case "CHARGED" :
                        Package = lugap.Charged((NetPackage)Package);
                        break;
                    case "VERIFIED" :
                        Package = lugap.Verified((NetPackage)Package);
                        break;
                    case "COMMENT" :
                        Package = lugap.Comment((NetPackage)Package);
                        break;
                    default :
                        System.out.println("Command not found...");
                        Package.clearArgs();
                }
                
                DOS.write(Package.getNetworkBytes());
                
            } catch (IOException e) {
                System.out.println(e);
                Continue = false;
            }
        }
    }
}
