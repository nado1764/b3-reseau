#ifndef PACKAGECHECKTICKET_H_INCLUDED
#define PACKAGECHECKTICKET_H_INCLUDED

#include <string>

#include "Package.hpp"


namespace CIMP {
  class PackageCheckTicket : public Package {
    private:
      PackageCheckTicket();
    public:
      //CONSTRUCTORS
      PackageCheckTicket(std::string Billet, std::string NbAccomp);

      //GETTERS
      std::tuple<std::string, std::string> getParams();
  };
}

#endif //PACKAGECHECKTICKET_H_INCLUDED
