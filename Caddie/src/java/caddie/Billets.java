/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caddie;

import BeanDBAccess.BeanDBAccessMariadb;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nado
 */
public class Billets {
    private Billets() {}
    
    public static List<Billet> getBillets(int nb) {
        List<Billet> l = new ArrayList<>();
        
        BeanDBAccessMariadb AccessBean;
        
        try {
            AccessBean = DBWrapper.getCtx();
        
            ResultSet rs = AccessBean.ExecuteQuery("select billets.id, vol, pays.nom, depart, arrivee from billets left join vols on vols.id = billets.vol left join pays on destination = pays.id" +
                                                    " where depart >= sysdate()"
                                                    + " and billets.status is null"
                                                    + " limit " + nb + ";");

            while(rs.next()) {
                l.add(new Billet(rs.getInt("id"), rs.getInt("vol"), rs.getString("nom"), rs.getDate("depart"), rs.getDate("arrivee")));
            }
        } catch (SQLException | ClassNotFoundException ex) {
        }
        return l;
    }
    
    public static List<Billet> getBilletsReserves(int idClient) {
        List<Billet> l = new ArrayList<>();
        
        BeanDBAccessMariadb AccessBean;
        
        try {
            AccessBean = DBWrapper.getCtx();
        
            ResultSet rs = AccessBean.ExecuteQuery("select billets.id, vol, pays.nom, depart, arrivee from billets left join vols on vols.id = billets.vol left join pays on destination = pays.id" +
                                                    " where passager = " + idClient + " and status = 'RESERVED'"
                                                    + ";");

            while(rs.next()) {
                l.add(new Billet(rs.getInt("id"), rs.getInt("vol"), rs.getString("nom"), rs.getDate("depart"), rs.getDate("arrivee")));
            }
        } catch (SQLException | ClassNotFoundException ex) {
        }
        return l;
    }
}
