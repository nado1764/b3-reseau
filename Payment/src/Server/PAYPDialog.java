/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import NetPackage.NetDialog;
import NetPackage.NetPackage;
import PAYP.PAYPServer;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import CryptoUtils.KSMgr;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;

/**
 *
 * @author Fluxio
 */
public class PAYPDialog extends NetDialog{
    PAYPServer payp;
    final KSMgr      keysMgr;
    final PrivateKey privKey;
    final PublicKey  pubKey;

    public PAYPDialog(Socket connection) throws IOException, KeyStoreException, NoSuchAlgorithmException, CertificateException, UnrecoverableKeyException, SQLException, ClassNotFoundException {
        super(connection);
        payp = new PAYPServer();
        keysMgr = new KSMgr("pay.ks", "paypay");
        privKey = keysMgr.getPrivateKey("pay", "paypay");
        pubKey  = keysMgr.getPublicKey("client");
    }
    
    @Override
    public void run() {
        Continue = true;
        NetPackage Package;
        Package = new NetPackage();
        
        try {
            Package.clearArgs();
            System.out.println("[*] Waiting for a request...");


            Package.readNetworkString(BR.readLine());
            System.out.println("[+] Received package : " + Package.getNetworkString());
            
            if(!((NetPackage)Package).getArg(0).equals("PAY")) {
                throw new Exception("Requete PAY attendue...");
            }

            String cryptedCardNumber = Package.getArg(1);
            String name              = Package.getArg(2);
            String amount            = Package.getArg(3);
            String signature         = Package.getArg(4);

            keysMgr.asymVerifSign(pubKey, cryptedCardNumber + name + amount, signature);
            
            String cardNumber = keysMgr.asymDecrypt(privKey, cryptedCardNumber);

            Package = payp.Pay(cardNumber, name, Float.parseFloat(amount));
             
        } catch (Exception e) {
            Package.clearArgs();
            e.printStackTrace();
        } finally {
            try {
                System.out.println("[*] Sending: " + Package.getNetworkString());
                DOS.write(Package.getNetworkBytes());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
