#ifndef TCPNEGOCIATION_HPP
#define TCPNEGOCIATION_HPP

#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>

#include <string>

#include "TCPConnection.hpp"

namespace tcpc {

class TCPNegociation : virtual public TCPConnection {
	protected:
		struct in_addr     m_ipAddress;
		in_port_t          m_port;
		struct sockaddr_in m_socketAddress;

		// Private methods
		bool initSocket();
		bool getHost(const std::string &host);

	public:
		TCPNegociation(const std::string &host, const uint16_t port);
};

}

#endif
