#include "constants.hpp"
#include "PackageCheckTicket.hpp"
#include "ConfParser/ConfParser.h"

#include <tuple>

using namespace std;

namespace CIMP {
  PackageCheckTicket::PackageCheckTicket() {
    setType(MsgType::CHECK_TICKET);
  }

  PackageCheckTicket::PackageCheckTicket(string Billet, string NbAccomp) {
    setType(MsgType::CHECK_TICKET);

    Args.clear();
  	Args.push_back(Billet);
  	Args.push_back(NbAccomp);
  }

  tuple<string, string> PackageCheckTicket::getParams() {
  	return make_tuple(Args[0], Args[1]);
  }
}
