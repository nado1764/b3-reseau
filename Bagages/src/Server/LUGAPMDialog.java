/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import LUGAP.LUGAPMServer;
import NetPackage.NetPackage;
import java.io.IOException;
import java.net.Socket;
import java.sql.SQLException;
import NetPackage.INetPackage;
import NetPackage.NetDialog;




public class LUGAPMDialog extends NetDialog {
    LUGAPMServer lugap;
    
    public LUGAPMDialog(Socket connection) throws IOException, SQLException, ClassNotFoundException {
        super(connection);
        lugap = new LUGAPMServer();
    }

    @Override
    public void run() {
        Continue = true;
        Boolean loggedin = false;
        
        INetPackage reqPkg = new NetPackage();
        while(Continue) { //TODO : implementer un cas de sortie de boucle : timeout/disconnect
            INetPackage answerPkg = null;
            
            reqPkg.clearArgs();
            
            try {                
                System.out.println("---");
                System.out.println("Waiting for a request...");
                
                reqPkg.readNetworkString(BR.readLine());
                System.out.println("Received package : " + reqPkg.getNetworkString());

                switch(((NetPackage)reqPkg).getArg(0)) {
                    case "LOGIN" :
                        answerPkg = lugap.Connect(((NetPackage)reqPkg));
                        break;
                    case "DISPLAYLUGGAGES":
                        answerPkg = lugap.DisplayLuggages((NetPackage)reqPkg);
                        break;
                    case "CHARGED":
                        answerPkg = lugap.SetCompleted((NetPackage)reqPkg);
                        break;
                    default :
                        answerPkg = new NetPackage();
                        System.out.println("Command not found...");
                }
                
                if(answerPkg != null)
                    DOS.write(answerPkg.getNetworkBytes());
                
            } catch (SQLException | IOException e) {
                System.out.println(e);
                Continue = false;
            }
        }
        //lugap.Connect(message);
    }
    
}
