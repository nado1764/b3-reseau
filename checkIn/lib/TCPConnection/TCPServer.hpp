#ifndef TCPSERVER_HPP
#define TCPSERVER_HPP

#include <stdint.h>

#include "TCPNegociation.hpp"
// needed to use the class
#include "TCPService.hpp"

namespace tcpc {

class TCPServer : TCPNegociation {
	private:
		int m_maxConns;
	public:
		using TCPNegociation::TCPNegociation;

		void setMaxConns(uint16_t max);
		int  getMaxConns(void);

		bool        Listen(bool blocking = false);
		TCPService* Accept();
};

}

#endif
