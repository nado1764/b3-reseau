#ifndef PACKAGELOGOUT_H_INCLUDED
#define PACKAGELOGOUT_H_INCLUDED

#include "Package.hpp"

namespace CIMP {
  class PackageLogout : public Package {
    public:
      PackageLogout();
  };
}

#endif //PACKAGELOGOUT_H_INCLUDED
