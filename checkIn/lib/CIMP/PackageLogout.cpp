#include "constants.hpp"
#include "PackageLogout.hpp"

namespace CIMP {
  PackageLogout::PackageLogout() {
    setType(MsgType::LOGOUT);
  }
}
