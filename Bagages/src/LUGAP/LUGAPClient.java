/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package LUGAP;

import NetPackage.NetPackage;

/**
 *
 * @author Fluxio
 */
public class LUGAPClient {
    NetPackage Package;
    
    public LUGAPClient() {
        Package = new NetPackage();
    }
    
    //DISPLAY PROTOCOL
    public NetPackage DisplayFlights() {
        Package.clearArgs();
        Package.appendArg("DISPLAYFLIGHTS");
        
        return Package;
    }
    
    public NetPackage DisplayLuggages(String FlightID) {
        Package.clearArgs();
        Package.appendArg("DISPLAYLUGGAGES");
        Package.appendArg(FlightID);
        
        return Package;
    }
    
    
    
    //REQUEST/UPD PROTOCOL
    public NetPackage Connect(String login, String password, String salt) {
        Package.clearArgs();
        Package.appendArg("CONNECT");
        Package.appendArg(login);
        Package.appendArg(password);
        Package.appendArg(salt);
         
        return Package;
    }
    
    public NetPackage Received(String IdLuggage, boolean isReceipt) {
        Package.clearArgs();
        Package.appendArg("RECEIVED");
        Package.appendArg(IdLuggage);
        Package.appendArg(isReceipt? "TRUE" : "FALSE");

        return Package;
    }
    
    public NetPackage Charged(String IdLuggage, boolean isCharged) {
        Package.clearArgs();
        Package.appendArg("CHARGED");
        Package.appendArg(IdLuggage);
        Package.appendArg(isCharged? "TRUE" : "FALSE");

        return Package;
    }
    
    public NetPackage Verified(String IdLuggage, boolean isVerified) {
        Package.clearArgs();
        Package.appendArg("VERIFIED");
        Package.appendArg(IdLuggage);
        Package.appendArg(isVerified? "TRUE" : "FALSE");

        return Package;
    }
    
    public NetPackage Comment(String IdLuggage, String Comment) {
        Package.clearArgs();
        Package.appendArg("COMMENT");
        Package.appendArg(IdLuggage);
        Package.appendArg(Comment);
        
        return Package;
    }
}
