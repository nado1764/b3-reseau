#include <cstdio>
#include <csignal>

#include <atomic>
#include <chrono>
#include <iostream>
#include <memory>
#include <string>
#include <thread>
#include <tuple>

#include "TCPConnection/TCPClient.hpp"
#include "ConfParser/ConfParser.h"
#include "CIMP/Session.hpp"
#include "CIMP/packages.hpp"

#include "userio.hpp"

using namespace std;
using namespace tcpc;

extern volatile atomic<bool> sigCaught(false);

void cimp_session(TCPClient *client);

void signalHandler(int signum) {
	sigCaught = true;
}

int main() {
	signal(SIGINT, signalHandler);

	ConfParser conf("checkin.ini");
	conf.AddProp("port", "4242");
	conf.AddProp("server", "127.0.0.1");
	conf.writeProps();

	TCPClient* client(nullptr);
	try {
		client = new TCPClient(conf["server"], atoi(conf["port"].c_str()));
	}
	catch(string err) {
		cout << "[-] Error : " << err << endl;
		return 1;
	}

	if(!client->Connect()) {
		cerr << "[-] Could not connect to server ";
		cerr << conf["server"] << ":" << conf["port"] << endl;
		return 1;
	}
	cout << "[+] Connected to server" << endl;

	thread t(cimp_session, client);

	while(!sigCaught) {
		this_thread::sleep_for(chrono::seconds(1));
	}

	cout << "\n[*] Closing application" << endl;

	t.join();

	cout << "\n[*] Closing connection" << endl;

	return 0;
}

void cimp_session(TCPClient* client) {
	using namespace CIMP;

	auto s = shared_ptr<TCPService>(client);
	Session session(s);
	s.reset();

	bool loggedIn = false;

	while(!loggedIn) {
		if(sigCaught || !session.sendLogin(askCredentials())) {
			cerr << "[-] Erreur d'envoi du login" << endl;
			return;
		}

		Package *pkg = session.getMessage();
		if(pkg == nullptr) {
			cerr << "[-] Package vide (nullptr)" << endl;
			continue;
		}

		if(pkg->getType() == MsgType::LOGIN) {
			PackageLogin *p = static_cast<PackageLogin*>(pkg);
			if(get<0>(p->getParams()) == "Y") {
				loggedIn = true;
			}
			else
				cout << "[-] Login invalide" << endl;
		}
		delete pkg;
	}

	cout << "[+] Login réussi" << endl;
	// logged in
	while(!sigCaught && loggedIn) {
		// check ticket
		MsgType t = loggedin_menu();
		if(t != MsgType::CHECK_TICKET) {
			cout << "[+] Logging out"<<endl;
			loggedIn = false;
			session.sendLogout();
			break;
		}

		// send check ticket
		session.sendTicket(askTicket());
		if(!session.getAnswer(MsgType::CHECK_TICKET)) {
			cout << "[-] Ticket invalide"<<endl;
			continue;
		}

		cout << "[+] Ticket validé" << endl;

		// check bagages
		auto lggArray = unique_ptr<vector<PackageLuggage>>(askLuggages());
		if(lggArray == nullptr) continue;

		for(auto p : *lggArray) {
			cout << p.getMsg() << endl;
			session.sendLuggage(p);
		}
		session.sendCheckLuggage();

		if(!session.getAnswer(MsgType::CHECK_LUGGAGE)) {
			cout << "[-] Bagages incorrects"<<endl;
			continue;
		}

		cout << "[+] Bagages acceptés"<<endl;
		// check paiement
		this_thread::sleep_for(chrono::milliseconds(10));
	}

	sigCaught = true;
}
