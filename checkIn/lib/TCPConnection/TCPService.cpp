#include <sys/types.h>
#include <sys/socket.h>

#include <string>

#include "TCPService.hpp"

namespace tcpc {

	TCPService::TCPService(int socketFD) {
		m_socketFD = socketFD;
	}

	bool TCPService::write(const char *msg, size_t len) const {
		if(!isAlive() || send(m_socketFD, msg, len, 0) == -1) {
			// check errno
			return false;
		}

		return true;
	}

	bool TCPService::write(const std::string &str) const {
		return write(str.c_str(), str.length());
	}

	std::string* TCPService::read(size_t strSize = 512) const {
		if(!isAlive()) {
			return nullptr;
		}

		char *buf = (char*)malloc(strSize+1);

		if(recv(m_socketFD, buf, strSize, 0) == -1) {
			free(buf);

			// check errnoS

			return nullptr;
		}

		std::string *str = new std::string(buf);
		free(buf);
		return str;
	}

}
