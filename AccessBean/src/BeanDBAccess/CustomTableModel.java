/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BeanDBAccess;

import java.util.Vector;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Fluxio
 */
public class CustomTableModel extends DefaultTableModel {
    
    public void removeColumn(int column) {
        // for each row, remove the column
        Vector rows = dataVector;
        for (Object row : rows) {
            ((Vector) row).remove(column);
        }

        // remove the header
        columnIdentifiers.remove(column);

        // notify
        fireTableStructureChanged();
    }
}
