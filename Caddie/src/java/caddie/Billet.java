/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caddie;

import java.sql.Date;
/**
 *
 * @author nado
 */
public class Billet {
    public int id;
    public int vol;
    public String dest;
    public Date depart;
    public Date arrivee;
    
    Billet(int id, int vol, String dest, Date depart, Date arrivee) {
        this.id = id;
        this.vol = vol;
        this.dest = dest;
        this.depart = depart;
        this.arrivee = arrivee;
    }
}
