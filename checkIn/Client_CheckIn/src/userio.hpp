#ifndef USERIO_HPP
#define USERIO_HPP

#include <string>
#include <tuple>
#include <vector>

#include "CIMP/constants.hpp"
#include "CIMP/PackageLuggage.hpp"

std::tuple<std::string,std::string> askCredentials();
std::tuple<std::string,int>         askTicket();
std::vector<CIMP::PackageLuggage>*         askLuggages();

CIMP::MsgType loggedin_menu();

#endif /* USERIO_HPP */
