/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BeanDBAccess;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Fluxio
 */
public class BeanDBAccessMariadb extends BeanDBAccess{
    /******************************/
    /* STATIC FINAL               */
    /******************************/
    public static String DriverPropertiesFileName = "DBDriverProperties.properties";
    
    /******************************/
    /* VARIABLES                  */
    /******************************/
    public ResultSet Rs;
    
    /******************************/
    /* CONSTRUCTOR                */
    /******************************/
    public BeanDBAccessMariadb() throws IOException {
        super();
        this.Driver = "org.mariadb.jdbc.Driver";
    }
    
    public BeanDBAccessMariadb(String Host, String Port, String Login, String Password, String Schema) {
        super(Host, Port, Login, Password, Schema);
        this.Driver = "org.mariadb.jdbc.Driver";
    }

    @Override
    public void Connect() throws SQLException, ClassNotFoundException {
        String Url = "jdbc:mariadb://" + getHost() + ":" + getPort() + "/" + getSchema() +
                        "?user=" + getLogin() +
                        "&password=" + getPassword();
        
        Class.forName(getDriver());

        setConnection((Connection) DriverManager.getConnection(Url));

        setStatement((Statement) Connection.createStatement());
    }
    
}
