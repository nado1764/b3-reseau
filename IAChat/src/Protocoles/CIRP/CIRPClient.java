/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Protocoles.CIRP;

import NetPackage.NetPackage;

/**
 *
 * @author Fluxio
 * Chat Info Retrieval Protocol
 */
public class CIRPClient {
    private static NetPackage Package;
    
    static {
        Package = new NetPackage();
    }
    
    //REQUEST/UPD PROTOCOL
    public static NetPackage Connect(String login, String password, String salt) {
        Package.clearArgs();
        Package.appendArg("CONNECT");
        Package.appendArg(login);
        Package.appendArg(password);
        Package.appendArg(salt);
         
        return Package;
    }
    
    //Methode probablement inutile
    public static NetPackage GetChatInfo() {
        Package.clearArgs();
        Package.appendArg("GETCHATINFO");
        
        return Package;
    }
}
