#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#include <string>

namespace CIMP {
	enum class MsgType {
		ERROR,
		LOGIN,
		LOGOUT,
		CHECK_TICKET,
		CHECK_LUGGAGE,
		LUGGAGE,
		PAYMENT_DONE
	};

	std::string MsgTypetoString(MsgType m);
	bool isNextState(MsgType Previous, MsgType Next);
}

#endif /* CONSTANTS_HPP */
