-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Lun 03 Septembre 2018 à 18:16
-- Version du serveur :  5.7.14
-- Version de PHP :  7.0.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `airport`
--
CREATE DATABASE IF NOT EXISTS `airport` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_520_ci;
USE `airport`;

create table transactionslog(comment varchar(128), success boolean);

-- --------------------------------------------------------

--
-- Structure de la table `agents`
--

CREATE TABLE `agents` (
  `Id` int(10) UNSIGNED NOT NULL,
  `TypeAgent` enum('AGENT_COMPAGNIE_AERIENNE','BAGAGISTE','TOUR_OPERATOR','AIGUILLEUR_DU_CIEL') COLLATE utf8_unicode_520_ci NOT NULL DEFAULT 'AGENT_COMPAGNIE_AERIENNE',
  `Nom` varchar(255) COLLATE utf8_unicode_520_ci NOT NULL,
  `Prenom` varchar(255) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `Password` varchar(255) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `Sexe` enum('H','F') COLLATE utf8_unicode_520_ci NOT NULL DEFAULT 'H',
  `Naissance` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Contenu de la table `agents`
--

INSERT INTO `agents` (`Id`, `TypeAgent`, `Nom`, `Prenom`, `Password`, `Sexe`, `Naissance`) VALUES
(1, 'AGENT_COMPAGNIE_AERIENNE', 'Warling', 'Clemence', NULL, 'F', '1993-08-25'),
(2, 'AGENT_COMPAGNIE_AERIENNE', 'Samyn', 'Axel', 'Samyn', 'H', '1993-01-11'),
(3, 'AGENT_COMPAGNIE_AERIENNE', 'Dervaux', 'Thibaut', NULL, 'H', '1992-04-01'),
(4, 'AGENT_COMPAGNIE_AERIENNE', 'Samyn', 'Antoine', NULL, 'H', '2000-03-20'),
(5, 'AGENT_COMPAGNIE_AERIENNE', 'Warling', 'Robin', NULL, 'H', '1996-06-28'),
(6, 'BAGAGISTE', 'Decordes', 'Jehan', NULL, 'H', '1993-07-18'),
(7, 'BAGAGISTE', 'Divers', 'Ludo', NULL, 'H', '1991-06-26'),
(8, 'TOUR_OPERATOR', 'Sarlet', 'Nathalie', NULL, 'F', '1964-04-20'),
(9, 'TOUR_OPERATOR', 'Tison', 'Anabelle', NULL, 'F', '1993-08-07'),
(10, 'AIGUILLEUR_DU_CIEL', 'Boremans', 'Allan', NULL, 'H', '1992-03-14'),
(11, 'AIGUILLEUR_DU_CIEL', 'Claessens', 'Mathieu', NULL, 'H', '1992-04-04'),
(12, 'AIGUILLEUR_DU_CIEL', 'Laverdeur', 'Clarice', NULL, 'F', '1993-09-20');

-- --------------------------------------------------------

--
-- Structure de la table `avions`
--

CREATE TABLE `avions` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Etat` enum('HANGAR','PISTE','CHECK_IN','VOL','CHECK_OUT') COLLATE utf8_unicode_520_ci NOT NULL DEFAULT 'HANGAR'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Contenu de la table `avions`
--

INSERT INTO `avions` (`Id`, `Etat`) VALUES
(1, 'HANGAR'),
(2, 'HANGAR'),
(3, 'HANGAR'),
(4, 'HANGAR'),
(5, 'HANGAR'),
(6, 'HANGAR'),
(7, 'HANGAR');

-- --------------------------------------------------------

--
-- Structure de la table `bagages`
--

CREATE TABLE `bagages` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Poids` float NOT NULL DEFAULT '0',
  `Type` enum('VALISE','AUTRE') COLLATE utf8_unicode_520_ci NOT NULL DEFAULT 'AUTRE',
  `Received` tinyint(1) NOT NULL DEFAULT '0',
  `Charged` tinyint(1) NOT NULL DEFAULT '0',
  `Verified` tinyint(1) NOT NULL DEFAULT '0',
  `Comment` varchar(512) COLLATE utf8_unicode_520_ci NOT NULL DEFAULT 'NEANT'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Contenu de la table `bagages`
--

INSERT INTO `bagages` (`Id`, `Poids`, `Type`, `Received`, `Charged`, `Verified`, `Comment`) VALUES
(1, 15, 'VALISE', 1, 1, 0, 'Test'),
(2, 13, 'VALISE', 1, 0, 0, 'NEANT'),
(3, 12.4, 'AUTRE', 0, 0, 0, 'NEANT'),
(4, 5, 'VALISE', 0, 0, 0, 'NEANT'),
(5, 25, 'VALISE', 0, 0, 0, 'NEANT'),
(6, 9.2, 'AUTRE', 0, 0, 0, 'NEANT'),
(7, 14.3, 'AUTRE', 0, 0, 0, 'NEANT'),
(8, 31, 'VALISE', 0, 0, 0, 'NEANT'),
(9, 5, 'AUTRE', 0, 0, 0, 'NEANT'),
(10, 18, 'VALISE', 0, 0, 0, 'NEANT');

-- --------------------------------------------------------

--
-- Structure de la table `billets`
--

CREATE TABLE `billets` (
  `Id` int(10) UNSIGNED NOT NULL COMMENT 'peut se changer par une clef composite : Passager + Vol',
  `Passager` int(10) UNSIGNED DEFAULT NULL,
  `Vol` int(10) UNSIGNED NOT NULL,
  `status` enum('RESERVED','PAID') COLLATE utf8_unicode_520_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Contenu de la table `billets`
--

INSERT INTO `billets` (`Id`, `Passager`, `Vol`, `status`) VALUES
(1, NULL, 1, NULL),
(2, NULL, 1, NULL),
(3, NULL, 2, NULL),
(4, NULL, 3, NULL),
(5, 9, 4, 'RESERVED'),
(6, NULL, 5, NULL),
(7, NULL, 2, NULL),
(8, NULL, 3, NULL),
(9, NULL, 4, NULL),
(10, NULL, 4, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `billetsbagages`
--

CREATE TABLE `billetsbagages` (
  `IdBillet` int(10) UNSIGNED NOT NULL,
  `IdBagage` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Contenu de la table `billetsbagages`
--

INSERT INTO `billetsbagages` (`IdBillet`, `IdBagage`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(1, 8),
(1, 9),
(1, 10);

-- --------------------------------------------------------

--
-- Structure de la table `passagers`
--

CREATE TABLE `passagers` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Nom` varchar(255) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `Prenom` varchar(255) COLLATE utf8_unicode_520_ci DEFAULT NULL,
  `password` varchar(20) COLLATE utf8_unicode_520_ci NOT NULL,
  `Naissance` date DEFAULT NULL,
  `Sexe` enum('H','F') COLLATE utf8_unicode_520_ci NOT NULL DEFAULT 'H',
  `Nationalite` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `username` varchar(12) COLLATE utf8_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Contenu de la table `passagers`
--

INSERT INTO `passagers` (`Id`, `Nom`, `Prenom`, `password`, `Naissance`, `Sexe`, `Nationalite`, `username`) VALUES
(1, 'Pazdera', 'Corentin', 'pazderac', '0000-00-00', 'H', 1, 'pazderac'),
(9, NULL, NULL, 'nado', NULL, 'H', 0, 'nado'),
(10, 'Samyn', 'Axel', 'Fluxio', '1993-01-11', 'H', 3, 'Fluxio');

-- --------------------------------------------------------

--
-- Structure de la table `pays`
--

CREATE TABLE `pays` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Nom` varchar(255) COLLATE utf8_unicode_520_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Contenu de la table `pays`
--

INSERT INTO `pays` (`Id`, `Nom`) VALUES
(4, 'Allemagne'),
(1, 'Belgique'),
(3, 'Danemark'),
(5, 'Espagne'),
(2, 'France'),
(8, 'Grece'),
(0, 'Inconnu'),
(9, 'Ireland'),
(6, 'Italie'),
(10, 'Pologne'),
(7, 'Royaume-Unis');

-- --------------------------------------------------------

--
-- Structure de la table `vols`
--

CREATE TABLE `vols` (
  `Id` int(10) UNSIGNED NOT NULL,
  `Destination` int(10) UNSIGNED NOT NULL COMMENT 'Fk Id pays',
  `Depart` datetime NOT NULL,
  `Arrivee` datetime NOT NULL,
  `ArriveeReelle` datetime DEFAULT NULL,
  `Avion` int(10) UNSIGNED NOT NULL COMMENT 'Fk id Avion',
  `Places` int(10) NOT NULL,
  `Prix` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_520_ci;

--
-- Contenu de la table `vols`
--

INSERT INTO `vols` (`Id`, `Destination`, `Depart`, `Arrivee`, `ArriveeReelle`, `Avion`, `Places`, `Prix`) VALUES
(1, 1, '2018-01-02 10:30:00', '2018-01-02 01:00:00', NULL, 1, 70, 733.33),
(2, 2, '2018-01-01 08:10:00', '2018-01-01 10:40:00', '2017-10-09 11:08:22', 3, 65, 600),
(3, 4, '2018-01-02 11:40:00', '2018-01-02 18:20:00', NULL, 5, 100, 999.99),
(4, 8, '2018-01-16 03:20:00', '2018-01-16 10:55:00', '2018-01-16 10:53:00', 2, 85, 75),
(5, 4, '2017-10-17 00:00:00', '2017-10-17 03:20:00', NULL, 5, 60, 200),
(6, 1, '2018-01-22 08:11:40', '2018-01-23 00:00:00', NULL, 2, 99, 169.99),
(7, 2, '2018-01-24 00:17:08', '2018-01-25 00:00:00', NULL, 6, 150, 815.5);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `agents`
--
ALTER TABLE `agents`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `avions`
--
ALTER TABLE `avions`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `bagages`
--
ALTER TABLE `bagages`
  ADD PRIMARY KEY (`Id`);

--
-- Index pour la table `billets`
--
ALTER TABLE `billets`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Passager` (`Passager`),
  ADD KEY `Vol` (`Vol`),
  ADD KEY `Passager_2` (`Passager`);

--
-- Index pour la table `billetsbagages`
--
ALTER TABLE `billetsbagages`
  ADD PRIMARY KEY (`IdBillet`,`IdBagage`),
  ADD KEY `FK_BILLETSBAGAGES_BAGAGES` (`IdBagage`);

--
-- Index pour la table `passagers`
--
ALTER TABLE `passagers`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `Nationalite` (`Nationalite`);

--
-- Index pour la table `pays`
--
ALTER TABLE `pays`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `Nom` (`Nom`);

--
-- Index pour la table `vols`
--
ALTER TABLE `vols`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Destination` (`Destination`),
  ADD KEY `Avion` (`Avion`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `agents`
--
ALTER TABLE `agents`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT pour la table `avions`
--
ALTER TABLE `avions`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `bagages`
--
ALTER TABLE `bagages`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `billets`
--
ALTER TABLE `billets`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'peut se changer par une clef composite : Passager + Vol', AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `passagers`
--
ALTER TABLE `passagers`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `pays`
--
ALTER TABLE `pays`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `vols`
--
ALTER TABLE `vols`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `billets`
--
ALTER TABLE `billets`
  ADD CONSTRAINT `FK_BILLETS_PASSAGERS` FOREIGN KEY (`Passager`) REFERENCES `passagers` (`Id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_BILLETS_VOLS` FOREIGN KEY (`Vol`) REFERENCES `vols` (`Id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `billetsbagages`
--
ALTER TABLE `billetsbagages`
  ADD CONSTRAINT `FK_BILLETSBAGAGES_BAGAGES` FOREIGN KEY (`IdBagage`) REFERENCES `bagages` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_BILLETSBAGAGES_BILLETS` FOREIGN KEY (`IdBillet`) REFERENCES `billets` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `passagers`
--
ALTER TABLE `passagers`
  ADD CONSTRAINT `FK_PASSAGERS_PAYS` FOREIGN KEY (`Nationalite`) REFERENCES `pays` (`Id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Contraintes pour la table `vols`
--
ALTER TABLE `vols`
  ADD CONSTRAINT `FK_VOLS_AVIONS` FOREIGN KEY (`Avion`) REFERENCES `avions` (`Id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `FK_VOLS_PAYS` FOREIGN KEY (`Destination`) REFERENCES `pays` (`Id`) ON DELETE NO ACTION ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
