#ifndef TCPCONNECTION_HPP
#define TCPCONNECTION_HPP

namespace tcpc {
class TCPConnection {
	protected:
		int m_socketFD;
		bool isAlive() const;
	public:
		~TCPConnection();
};
}

#endif /* TCPCONNECTION_HPP */
