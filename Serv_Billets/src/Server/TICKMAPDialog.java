/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import NetPackage.NetDialog;
import NetPackage.NetPackage;
import TICKMAP.TICKMAPServer;
import java.io.IOException;
import java.net.Socket;
import NetPackage.INetPackage;
import java.security.PrivateKey;
import java.sql.SQLException;
import CryptoUtils.KSMgr;
import PAYP.PAYPClient;
import TICKMAP.Config;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.security.PublicKey;

/**
 *
 * @author nado
 */
public class TICKMAPDialog extends NetDialog {
    TICKMAPServer       tickmap;
    KSMgr               keysMgr;
    
    Socket              socket;
    DataOutputStream    PayDOS;
    BufferedReader      PayBR;
    
    public TICKMAPDialog(Socket connection) throws IOException, SQLException, ClassNotFoundException {
        super(connection);
        tickmap = new TICKMAPServer();
    }
    
    private void startPayTCPConnection() {
        try {
            socket = new Socket(Config.getServerPay(), Config.getPortPay());
            
            PayDOS = new DataOutputStream(socket.getOutputStream());
            PayBR = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void run() {
        Continue = false;
        String hmac = "";
        INetPackage Package;
        try {
            keysMgr = new KSMgr("server.ks", "server");
            Package = new NetPackage();

            System.out.println("Waiting for a login");
            Package.readNetworkString(BR.readLine());
            System.out.println("[+] Paquet reçu : " + Package.getNetworkString());

            switch (((NetPackage)Package).getArg(0)) {
                case "CONNECT":
                    Package = tickmap.Connect((NetPackage)Package);
                    DOS.write(Package.getNetworkBytes());
                    if(((NetPackage)Package).getList().size() == 0) {
                        Continue = false;
                        System.err.println("[-] Connexion ratée");
                        break;
                    }
                    System.out.println("[+] Client connecté");
                    
                    // Handshake
                    // réception de la clé secrète du client utilisée pour la communication
                    // et de l’IV commun
                    // si le message n’est pas déchiffrable avec la clé privée du serveur, on coupe la connexion
                    PrivateKey privKey = keysMgr.getPrivateKey("server", "server");
                    System.out.println("get private key");

                    Package = new NetPackage();
                    String readLine = BR.readLine();
                    System.out.println("readLine : " + readLine);
                    String cryptLine = keysMgr.asymDecrypt(privKey, readLine);
                    System.out.println("cryptLine : " + cryptLine);
                    Package.readNetworkString(cryptLine);

                    String secretKey = ((NetPackage)Package).getArg(0);
                    String iv        = ((NetPackage)Package).getArg(1);
                    hmac             = ((NetPackage)Package).getArg(2);

                    keysMgr.setSessionKey(secretKey, iv);
                    System.out.println("[+] Clé secrète et iv ajoutés");

                    // Envoi de la liste de vols
                    Package = tickmap.DisplayFlights((NetPackage)Package);
                    DOS.write((keysMgr.sessionEncrypt(Package.getNetworkString(false)) + "\n").getBytes("UTF-8"));
                    System.out.println("[*] Liste de vols envoyée");
                    
                    Continue = true;
                    
                    break;
                case "PAYFORID":
                    PublicKey paykey = keysMgr.getPublicKey("caddie");
                    NetPackage p = (NetPackage)Package;
                    // si signature correcte
                    if(keysMgr.asymVerifSign(paykey, p.getArg(0) + p.getArg(1), p.getArg(2))) {
                        Package = tickmap.PayForId(p.getArg(1));
                        DOS.write(Package.getNetworkBytes());
                    }
                    else {
                        System.err.println("[-] Signature incorrecte de la part de l'application web");;
                    }
                    break;
                default:
                    System.out.println("We did not receive a CONNECT or PAYFORID package, closing the connection");
            }

            while(Continue) { //TODO : implementer un cas de sortie de boucle : timeout/disconnect
                Package = new NetPackage();

                System.out.println("Waiting for a request...");

                Package.readNetworkString(keysMgr.sessionDecrypt(BR.readLine()));

                System.out.println("Received package : " + Package.getNetworkString());

                switch(((NetPackage)Package).getArg(0)) {
                    
                    case "CONNEXION_RESERVATION" :
                        Package = tickmap.ConnexionReservation((NetPackage)Package);
                        break;
                    
                    case "INSCRIPTION_RESERVATION" :
                        Package = tickmap.InscriptionReservation((NetPackage)Package);
                        break;
                    
                    case "CONFIRMATION_RESERVATION" :
                        Package = tickmap.ConfirmerReservation((NetPackage)Package);
                        break;
                    case "PAIEMENT_RESERVATION" :
                        startPayTCPConnection();
                        PAYPClient payp = new PAYPClient();
                        // TODO: private key not found, check server.ks
                        String cardNum = ((NetPackage)Package).getArg(1);
                        String owner = ((NetPackage)Package).getArg(2);
                        String price = ((NetPackage)Package).getArg(3);
                        String signature = ((NetPackage)Package).getArg(4);
                        
                        Package = payp.Pay(cardNum, owner, price, signature);
                        System.out.println("[*] Sending payment requet");
                        PayDOS.write(Package.getNetworkBytes());
                        Package.clearArgs();
                        Package.readNetworkString(PayBR.readLine());
                        
                        if(((NetPackage)Package).getArg(0).equals("FAIL")) {
                            Package = new NetPackage("PAIEMENT_REFUSE");
                            
                            DOS.write((keysMgr.sessionEncrypt(Package.getNetworkString(false)) + "\n").getBytes("UTF-8"));
                            System.out.println("[-] Paiement échoué");
                        }
                        else {
                            Package = tickmap.ConfirmerPaiement(((NetPackage)Package).getArg(0).equals("OK"),
                                                                owner,
                                                                price
                                                                );
                            DOS.write((keysMgr.sessionEncrypt(Package.getNetworkString(false)) + "\n").getBytes("UTF-8"));
                        }
                        System.out.println("Package renvoyé : " + Package.getNetworkString());
                        break;
                    default :
                        System.out.println("Command not found...");
                        Package.clearArgs();
                }

                DOS.write((keysMgr.sessionEncrypt(Package.getNetworkString(false)) + "\n").getBytes("UTF-8"));
            }
        } catch (Exception e) {
            //System.out.println(e);
            e.printStackTrace();
        }
    }
}
