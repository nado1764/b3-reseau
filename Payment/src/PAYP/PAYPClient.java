/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package PAYP;

import NetPackage.NetPackage;

/**
 *
 * @author Fluxio
 */
public class PAYPClient {
    NetPackage Package;
    
    //CONSTRUCTORS
    public PAYPClient() {
        Package = new NetPackage();
    }
    
    public NetPackage Pay(String CryptCardNum, String Owner, String Price, String Signature) {
        Package.clearArgs();
        Package.appendArg("PAY");
        Package.appendArg(CryptCardNum);
        Package.appendArg(Owner);
        Package.appendArg(Price);
        Package.appendArg(Signature);
        
        return Package;
    }
    
    //Utile seulement dans le cas où la CONFIRMATION de paiement passe par l'intermédiaire du client, on contacte donc Serveur_Billets
    public NetPackage Confirm(String SignatureSerPay) {
        Package.clearArgs();
        Package.appendArg("CONFIRM");
        Package.appendArg(SignatureSerPay); //Si vide, c'est que le paiement n'a pas eu lieu (peut etre un msg d'erreur)
        
        return Package;
    }
}
