<%-- 
    Document   : JSPCaddie
    Created on : Dec 1, 2017, 9:10:32 PM
    Author     : nado
--%>
<%@page import="java.util.List"%>
<%@page import="caddie.Billet"%>
<%@page import="caddie.Billets"%>
<%@include file="WEB-INF/jspf/loginCheck.jspf"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="static/head.html"%>
        <title>JSP Caddie</title>
    </head>
    <body>
<%@include file="static/header.jsp"%>
        
        <h1>Choix des billets</h1>

        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Destination</th>
                        <th>Départ</th>
                        <th>Arrivée</th>
                        <th>Réservation</th>
                    </tr>
                </thead>
                <tbody>
                    <%
                    List<Billet> l = Billets.getBillets(20);
                    if(l != null) {
                    for(Billet b : l) {
                    %>
                    <tr>
                        <td><% out.print(b.id); %></td>
                        <td><% out.print(b.dest); %></td>
                        <td><% out.print(b.depart.toLocalDate()); %></td>
                        <td><% out.print(b.arrivee.toLocalDate()); %></td>
                        <td><a href="Caddie?billet=<% out.print(b.id);%>">Re</a></td>
                    </tr>
                    <%
                    }}
                    %>
                </tbody>
            </table>
        </div>

<%@include file="static/footer.html" %>