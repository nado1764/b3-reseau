#include <netdb.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string.h>

#include <string>

#include "TCPNegociation.hpp"


namespace tcpc {
TCPNegociation::TCPNegociation(const std::string& host, const uint16_t port) {
	if(!initSocket()) {
		fprintf(stderr, "[-] Failed to create socket\n");
		throw "Failed to create socket";
	}
	if(!getHost(host)) {
		fprintf(stderr, "[-] Failed to get host ip\n");
		throw "Failed to get host ip";
	}
	
	m_socketAddress.sin_family = AF_INET;
	m_socketAddress.sin_port = m_port = htons(port);
	m_socketAddress.sin_addr = m_ipAddress;
}

bool TCPNegociation::initSocket() {
	m_socketFD = socket(AF_INET, SOCK_STREAM, 0);
	return m_socketFD != -1;
}

bool TCPNegociation::getHost(const std::string& host) {
	struct hostent *tmpHosts;

	if(!(tmpHosts = gethostbyname(host.c_str()))) {
		return false;
	}

	memcpy(&m_ipAddress, tmpHosts->h_addr_list[0], tmpHosts->h_length);
	return true;
}

}
