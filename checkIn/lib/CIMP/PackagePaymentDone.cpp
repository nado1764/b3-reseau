#include "constants.hpp"
#include "Package.hpp"
#include "PackagePaymentDone.hpp"

namespace CIMP {
  PackagePaymentDone::PackagePaymentDone() {
    setType(MsgType::PAYMENT_DONE);
  }

}
