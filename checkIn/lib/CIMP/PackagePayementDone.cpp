#include "PackagePayementDone.hpp"
#include "constants.hpp"

namespace CIMP {
  PackagePayementDone::PackagePayementDone() {
    setType(MsgType::PAYMENT_DONE);
  }

}
