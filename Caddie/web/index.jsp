<%@include file="WEB-INF/jspf/loginCheck.jspf"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <%@include file="static/head.html"%>
        <link rel="stylesheet" href="css/JSPInit.css">
        <title>JSP Init</title>
    </head>
    <body>
        <%@include file="static/header.jsp"%>
        <div class="container">
            <div class="row" id="loginrow">
                <div class="col-md-4 center">
                    <div class="panel panel-default">
                        <div class="panel-heading centertext">
                            <h3 class="panel-title">Bienvenue</h3>
                        </div>
                        <div class="panel-body">
                            <form accept-charset="UTF-8" role="form" action="Login" method="POST">
                                <fieldset>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Login" name="username" type="text">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Mot de passe" name="password" type="password" value="">
                                    </div>
                                    <div class="checkbox centertext">
                                    <label>
                                        <input name="new" type="checkbox" value="true"> Nouveau client
                                    </label>
                                    </div>
                                    <input class="btn btn-lg btn-success btn-block" type="submit" value="Login">
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
<%@include file="static/footer.html" %>