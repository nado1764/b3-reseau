#include <iostream>

#include "packages.hpp"

namespace CIMP {
  using namespace std;

  Package::Package() {}
  Package::Package(const Package& P) {
    this->PackageType = P.getType();
    this->Separator = P.getSep();
  }

  //SETTERS
  void Package::setType(MsgType PackageType) {
    this->PackageType = PackageType;
  }

  void Package::setSep(char Separator) {
    this->Separator = Separator;
  }

  //GETTERS
  MsgType Package::getType() const {
    return this->PackageType;
  }

  char Package::getSep() const {
    return this->Separator;
  }

  string Package::getMsg() const {
    string msg = to_string(static_cast<int>(getType())) + getSep();

    for(auto S : Args) {
      msg += S + getSep();
    }

    return msg;
  }

  string Package::getArg(int i) {
	  if(this->Args.size() < i+1) return "";

	  return Args[i];
  }

  //METHODS
  Package* Package::Create(vector<string> readArgs, bool guess) {
	if(readArgs.empty())
		return new Package();

	MsgType type;

	try {
		int typeI = stoi(readArgs[0], nullptr);
		type = static_cast<MsgType>(typeI);
	}
	catch(exception e) {
      return new Package();
	}

	if(!guess) {
		Package *p;
		p = new Package();
		p->setType(type);

		for(int i=1; i < readArgs.size(); ++i) {
			p->Args.push_back(readArgs[i]);
		}
		return p;
	}

	Package *pkg = nullptr;

	switch (type) {
		case MsgType::LOGIN:
			pkg = static_cast<Package *>(new PackageLogin(readArgs[1], readArgs[2]));
			break;
		case MsgType::LOGOUT:
			pkg = static_cast<Package *>(new PackageLogout());
			break;
		case MsgType::CHECK_TICKET:
			pkg = static_cast<Package *>(new PackageCheckTicket(readArgs[1], readArgs[2]));
			break;
		case MsgType::CHECK_LUGGAGE:
			pkg = static_cast<Package *>(new PackageCheckLuggage());
			break;
		case MsgType::LUGGAGE:
			pkg = static_cast<Package *>(new PackageLuggage(stof(readArgs[0], nullptr), (readArgs[1] == "Y") ? true : false));
			  break;
		case MsgType::PAYMENT_DONE:
			pkg = static_cast<Package *>(new PackagePaymentDone());
			break;
		default:
			pkg = new Package();
	}
	cout <<"pkg construit " << pkg->getMsg()<<endl;
	return pkg;
  }

  //DESTRUCTOR
  Package::~Package() {}
}
