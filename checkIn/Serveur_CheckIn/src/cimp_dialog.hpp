#ifndef CIM__DIALOG_HPP
#define CIM__DIALOG_HPP

#include <memory>

#include "TCPConnection/TCPServer.hpp"
#include "TCPConnection/TCPService.hpp"

void cimp_listen(tcpc::TCPServer *server);
void cimp_session(std::shared_ptr<tcpc::TCPService> connection);

#endif /* CIM__DIALOG_HPP */
