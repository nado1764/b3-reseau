#include <csignal>
#include <atomic>
#include <chrono>
#include <thread>

#include "TCPConnection/TCPServer.hpp"
#include "ConfParser/ConfParser.h"

#include "cimp_dialog.hpp"

// declare var as volatile to tell the compiler that it may be changed from
// somewhere else in the code and prevents attempt to optimize things like
// while(sigCaught) to while(false)
extern volatile std::atomic<bool> sigCaught(false);

void signalHandler(int signum) {
	sigCaught = true;
}

int main() {
	signal(SIGINT, signalHandler);

	ConfParser conf("checkind.ini");
	conf.AddProp("port", "4242");
	conf.AddProp("server", "127.0.0.1");
	conf.AddProp("max_connections", "3");
	conf.writeProps();

	tcpc::TCPServer *server;
	try {
		server = new tcpc::TCPServer(conf["server"], atoi(conf["port"].c_str()));
	}
	catch(std::string err) {
		std::cout << "[-] Error : " << err << std::endl;
		return 1;
	}
	server->setMaxConns(atoi(conf["max_connections"].c_str()));

	std::cout << "[*] Starting server at " << conf["server"] << ":" << conf["port"] << std::endl;
	if(server->Listen(true)) {
		std::cout << "[+] Success" << std::endl;
	}
	else {
		std::cout << "[-] Failure, aborting" << std::endl;
		return 1;
	}

	// thread qui gères accept
	std::thread serverThread(cimp_listen, server);

	while(!sigCaught) {
		std::this_thread::sleep_for(std::chrono::seconds(2));
	}
	std::cout << '\n';
	std::cout << "[*] Signal caught, closing connections" << std::endl;

	delete server;

	serverThread.join();

	return 0;
}
