/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caddie;

import BeanDBAccess.BeanDBAccessMariadb;
import java.sql.SQLException;

/**
 *
 * @author nado
 */
public class DBWrapper {
    public static BeanDBAccessMariadb getCtx() throws SQLException, ClassNotFoundException {
        BeanDBAccessMariadb AccessBean = new BeanDBAccessMariadb("localhost", "3306", "default", "default", "airport");
        AccessBean.Connect();
        return AccessBean;
    }
}
