/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package caddie;

import BeanDBAccess.BeanDBAccessMariadb;
import BeanDBAccess.BeanDBAccessMySql;
import CryptoUtils.KSMgr;
import NetPackage.NetPackage;
import TICKMAP.TICKMAPClient;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author nado
 */
@WebServlet(name = "Caddie", urlPatterns = {"/Caddie"})
public class Caddie extends HttpServlet {
    
    // sets number of items in caddie for current logged in user in its session
    public static void getCaddieNb(HttpSession session, BeanDBAccessMariadb ctx, int passager) throws SQLException {
        ResultSet rs = ctx.ExecuteQuery("select count(*) from billets where passager = " + passager + " and status = 'RESERVED';");
        int b = 0;
        if(rs.next()) {
            b = rs.getInt(1);
        }
        session.setAttribute("caddie", b);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        
        if(session.getAttribute("id") == null) {
            response.sendRedirect("index.jsp");
            return;
        }
        String billet = request.getParameter("billet");

        if(billet != null) {
            BeanDBAccessMariadb AccessBean;
            try {
                AccessBean = DBWrapper.getCtx();
                int passager = (int)session.getAttribute("id");
                if(1 == AccessBean.ExecuteUpdate("update billets set passager=" + passager + ", status='RESERVED' where id = " + Integer.parseInt(billet) +";")) {
                    getCaddieNb(session, AccessBean, passager);
                }
            } catch (SQLException | ClassNotFoundException ex) {
                response.sendRedirect("JSPCaddie.jsp?error=" + URLEncoder.encode(ex.getMessage(), "utf8"));
            }
        }
        response.sendRedirect("JSPCaddie.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        if(request.getParameter("pay") != null) {
            
            // TODO: connect to serveur billets and pay reservation
            HttpSession session = request.getSession();
            BeanDBAccessMariadb AccessBean;
           
            
            int passager = (int)session.getAttribute("id");
            // ouvrir connexion à serveur billets
            // créer un paquet demandant la confirmation de paiement du passager à l’id x
            // signer paquet
            // envoyer paquet
            // renvoyer la réponse reçue par billetsà  l’utilisateur
            
            // connexion
            Socket     connection = new Socket("localhost", 7003);
            DataOutputStream  DOS = new DataOutputStream(connection.getOutputStream());
            BufferedReader     BR = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            
            // récupération de la clé privée et signature du paquet
            KSMgr keysMgr = null;
            try {
                // création du paquet
                TICKMAPClient tickmap = new TICKMAPClient();
                NetPackage    Package = tickmap.PayForPassengerId(passager);
                keysMgr = new KSMgr(getClass().getResourceAsStream("caddie.ks"), "caddie");
                PrivateKey key = keysMgr.getPrivateKey("caddie", "caddie");
                
                String signature = keysMgr.asymSign(key, Package.getArg(0) + Package.getArg(1));
                Package.appendArg(signature);
                // envoi du paquet
                DOS.write(Package.getNetworkBytes());
                
                // we dont care about the answer since it is a simple signal to say it is done
                BR.readLine();
                // refresh session entries

                AccessBean = DBWrapper.getCtx();
                getCaddieNb(session, AccessBean, passager);
            } catch (ClassNotFoundException|SQLException|IOException | InvalidKeyException | KeyStoreException | NoSuchAlgorithmException | SignatureException | UnrecoverableKeyException | CertificateException ex) {
                connection.close();
                response.sendRedirect("JSPPay.jsp?error=" + URLEncoder.encode(ex.getMessage(), "utf8"));
                return;
            }
        }
        response.sendRedirect("JSPCaddie.jsp");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
