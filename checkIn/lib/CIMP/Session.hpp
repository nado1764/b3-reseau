#ifndef SESSION_H_INCLUDED
#define SESSION_H_INCLUDED

#include <string>
#include <list>
#include <tuple>
#include <vector>

#include "TCPConnection/TCPService.hpp"

#include "packages.hpp"

namespace CIMP {
  class Session {
    private:
      //VARIABLES
      bool isAlive = true;
      bool IsLoggedIn = false;
      MsgType LastType = MsgType::ERROR;

	  std::shared_ptr<tcpc::TCPService> TServ;

    public:
      //VARIABLES
      std::vector<PackageLuggage> Luggages;

      //CONSTRUCTORS
      Session (std::shared_ptr<tcpc::TCPService> TServ);
      Session (const Session& S);

      //SETTERS
      void setLastType(MsgType LastType);
      void setTCPServ(std::shared_ptr<tcpc::TCPService> TServ);

      //GETTERS
      MsgType getLastType() const;
      std::shared_ptr<tcpc::TCPService> getTCPServ() const;

      //OPERATORS

      //METHODS
      bool isNextType(MsgType NextType) const;
      bool isLoggedIn() const;
      bool isLoggedIn(bool state);
  	  bool isActive();
  	  Package* getMessage(bool guess = true) const;
	  bool getAnswer(MsgType type);
	  bool sendAnswer(MsgType type, bool answer = true, std::string reason = "");
      bool sendLogin(std::string login, std::string password);
      bool sendLogin(std::tuple<std::string,std::string> credentials);
	  bool sendLogout();
	  bool sendTicket(std::tuple<std::string,int> ticket);
	  bool sendLuggage(PackageLuggage &pkg);
	  bool sendCheckLuggage();
      void die();

      //DESTRUCTOR
      ~Session() {};
  };
}

#endif //SESSION_H_INCLUDED
