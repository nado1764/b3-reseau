#ifndef PACKAGEPAYEMENTDONE
#define PACKAGEPAYEMENTDONE

#include "Package.hpp"

namespace CIMP {
  class PackagePayementDone : public Package {
    public:
      PackagePayementDone();
  };

}

#endif //PACKAGEPAYEMENTDONE
