#include <unistd.h>
#include <iostream>

#include "TCPConnection.hpp"

namespace tcpc {
	TCPConnection::~TCPConnection() {
		if(m_socketFD) {
			std::cout << "[+][tcpc] Closing connection" << std::endl;
			close(m_socketFD);
		}
	}

	bool TCPConnection::isAlive() const {
		return m_socketFD > 0;
	}
}
