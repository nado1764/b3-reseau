#include "constants.hpp"

namespace CIMP {
std::string MsgTypetoString(MsgType m)
{
	std::string s;
	switch(m) {
		case MsgType::LOGIN:
			s = "LOGIN";
			break;
		case MsgType::ERROR:
			s = "ERROR";
			break;
		case MsgType::LOGOUT:
			s = "LOGOUT";
			break;
		case MsgType::CHECK_TICKET:
			s = "CHECK_TICKET";
			break;
		case MsgType::CHECK_LUGGAGE:
			s = "CHECK_LUGGAGE";
			break;
		case MsgType::LUGGAGE:
			s = "LUGGAGE";
			break;
		case MsgType::PAYMENT_DONE:
			s = "PAYMENT_DONE";
			break;
	}

	return s;
}

bool isNextState(MsgType Previous, MsgType Next) {
	switch (Next) {
		case MsgType::LOGIN:
			return (Previous == MsgType::ERROR ||
					Previous == MsgType::LOGOUT ||
					Previous == MsgType::LOGIN);
		case MsgType::ERROR:
		case MsgType::LOGOUT:
			return true;
		case MsgType::CHECK_TICKET:
			return (Previous == MsgType::LOGIN ||
					Previous == MsgType::PAYMENT_DONE);
		case MsgType::LUGGAGE:
			return (Previous == MsgType::CHECK_TICKET ||
					Previous == MsgType::LUGGAGE);
		case MsgType::CHECK_LUGGAGE:
			return (Previous == MsgType::LUGGAGE);

		case MsgType::PAYMENT_DONE:
			return (Previous == MsgType::CHECK_LUGGAGE);
	}
	return false;
  }
}
