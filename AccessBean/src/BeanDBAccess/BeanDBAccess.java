/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BeanDBAccess;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Set;

/**
 *
 * @author Fluxio
 */
public abstract class BeanDBAccess implements Serializable {
    /******************************/
    /* STATIC FINAL               */
    /******************************/
    public static String BeanPropertiesFileName = "BeanDBAccessProperties.properties";
    
    /******************************/
    /* VARIABLES MEMBRES          */
    /******************************/
    protected String Driver;
    protected String Host;
    protected String Port;
    protected String Login;
    protected String Password;
    protected String Schema;
    protected Connection Connection;
    protected Statement Statement;
    
    /******************************/
    /* CONSTRUCTEURS              */
    /******************************/
    public BeanDBAccess() throws FileNotFoundException, IOException
    {
        /******************************/
        /* VARIABLES                  */
        /******************************/
        
        Properties BeanProp = new Properties();

        /******************************/
        /* INIT                       */
        /******************************/
        InputStream Input = new FileInputStream(BeanPropertiesFileName);

        /******************************/
        /* CORPS                      */
        /******************************/
        BeanProp.load(Input);

        this.Host = BeanProp.getProperty("Host");
        this.Port = BeanProp.getProperty("Port");
        this.Login = BeanProp.getProperty("Login");
        this.Password = BeanProp.getProperty("Password");
        this.Schema = BeanProp.getProperty("Schema");

        Input.close();
    }
    
    public BeanDBAccess(String Host, String Port, String Login, String Password, String Schema) {
        this.Host = Host;
        this.Port = Port;
        this.Login = Login;
        this.Password = Password;
        this.Schema = Schema;
    }
    
    /******************************/
    /* SETTERS & GETTERS          */
    /******************************/
    public void setDriver(String Driver) {
        this.Driver = Driver;
    }

    public void setHost(String Host) {
        this.Host = Host;
    }

    public void setPort(String Port) {
        this.Port = Port;
    }

    public void setLogin(String Login) {
        this.Login = Login;
    }

    public void setPassword(String Password) {
        this.Password = Password;
    }

    public void setSchema(String Schema) {
        this.Schema = Schema;
    }

    public void setConnection(Connection Connection) {
        this.Connection = Connection;
    }

    public void setStatement(Statement Statement) {
        this.Statement = Statement;
    }
    
    /******************************/
    
    public String getDriver() {
        return Driver;
    }

    public String getHost() {
        return Host;
    }

    public String getPort() {
        return Port;
    }

    public String getLogin() {
        return Login;
    }

    public String getPassword() {
        return Password;
    }

    public String getSchema() {
        return Schema;
    }

    public Connection getConnection() {
        return Connection;
    }

    public Statement getStatement() {
        return Statement;
    }  
    
    /******************************/
    /* METHODES                   */
    /******************************/
    public abstract void Connect() throws SQLException, ClassNotFoundException;
    
    public void Disconnect() throws SQLException{
        if(getStatement() != null)
        {
            getStatement().close();
        }
    }
    
    public ResultSet ExecuteQuery(String Query) throws SQLException {
        try {
            setStatement(getConnection().
                            createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY));
            return getStatement().executeQuery(Query);
        } catch (SQLException ex) {
            System.out.println(ex);
            this.Rollback();
            throw new SQLException(ex.toString() + " ("+ Query+")");
        }
    }
    
    public int ExecuteUpdate(String Query) throws SQLException {
        try {
            return getStatement().executeUpdate(Query);
        } catch (SQLException ex) {
            this.Rollback();
            throw ex;
        }
    }
    
    public boolean Execute(String Query) throws SQLException {
        try {
            return getStatement().execute(Query);
        } catch (SQLException ex) {
            this.Rollback();
            throw ex;
        }
    }
    
    public void SetAutoCommit(boolean AutoCommit) throws SQLException {
        try {
            getConnection().setAutoCommit(AutoCommit);
        } catch (SQLException ex) {
            this.Rollback();
            throw ex;
        }
    }
    
    public void Commit() throws SQLException {
        try {
            getConnection().commit();
        }
        catch(SQLException ex) {
            this.Rollback();
            throw ex;
        }
    }
    
    public void Rollback() throws SQLException
    {
        if(getConnection().getAutoCommit() == false)
            getConnection().rollback();
    }
    
    public synchronized int Ecriture(String From, HashMap Data) throws Exception {
        /******************************/
        /* VARIABLES                  */
        /******************************/
        String Url;
        String Champs;
        String Valeurs;
        Set Cles;
        Iterator Iter;
        
        Object Cle;
        
        PreparedStatement pSmt;
        ResultSet Rs;
        
        /******************************/
        /* INIT                       */
        /******************************/
        Champs = "(";
        Valeurs = "(";
        Cles = Data.keySet();
        Iter = Cles.iterator();
        
        /******************************/
        /* CORPS - CREATE QUERY       */
        /******************************/
        while(Iter.hasNext()) {
            Cle = Iter.next();
            
            Champs += Cle.toString();
            
            if(Data.get(Cle) instanceof Double || Data.get(Cle) instanceof Float)
                Valeurs += Data.get(Cle).toString();
            else
                Valeurs += "'" + Data.get(Cle).toString() + "'";
            
            if(Iter.hasNext()) {
                Champs += ",";
                Valeurs += ",";
            }
        }
        
        
        Champs += ")";
        Valeurs += ")";
        
        Url = "INSERT INTO " + From + Champs + " VALUES " + Valeurs;
        /******************************/
        
        pSmt = getConnection().prepareStatement(Url);
        pSmt.executeUpdate();
        
        Rs = pSmt.getGeneratedKeys();
        
        /* RETURN LASTLY_INSERTED_INDEX OR 0*/
        if(Rs.next()) {
            int LastlyInsertedId = Rs.getInt(1);
            return LastlyInsertedId;
        }
        
        return 0;
    }
}
