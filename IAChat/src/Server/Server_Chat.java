/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

/**
 *
 * @author Fluxio
 */
public class Server_Chat {
    Properties props;
    ServerSocket socket;

    public Server_Chat() {
        loadProps();
        
        startChatServer();
    }
    
    private void loadProps() {
        props = new Properties();
        FileInputStream file;
        
        try {
            file = new FileInputStream("ServerChat.properties");
            props.load(file);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
    
    private void startChatServer() {
        try {
            socket = new ServerSocket(Integer.parseInt(props.getProperty("Port", "7520")), 
                                      Integer.parseInt(props.getProperty("Max_Conns", "5")));
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }
    
    public void listen() {
        Thread clientsHandlerThread;
        clientsHandlerThread = new Thread() {
            @Override
            public void run() {
                while (true) {
                    try {
                        System.out.println("[*][listen] En attente de connexion...");
                        Socket connection = socket.accept();
                        
                        System.out.println("[+][listen] Nouvelle connexion.");
                        Thread clientHandlerThread = new Thread(new CIRPDialog(connection));
                        
                        clientHandlerThread.start();
                    } catch (IOException ex) {
                        System.out.println(ex);
                    }
                }
            }
        };
        
        clientsHandlerThread.start();
        
        while(clientsHandlerThread.isAlive() && clientsHandlerThread.isAlive()) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {}
        }
        
        if(clientsHandlerThread.isAlive()) {
            clientsHandlerThread.interrupt();
        }

        
        try {
            System.out.println("[*] Fermeture des connexions");
            socket.close();
        } catch (IOException ex) {
            System.err.println("[-] Erreur à la fermeture des sockets");
        }
    }
    
    public static void main(String[] args) {
        Server_Chat Serv = new Server_Chat();
        Serv.listen();
    }
}
