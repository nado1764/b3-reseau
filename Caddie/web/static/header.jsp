    <header>
        <nav class="navbar navbar-expand-md navbar-dark bg-dark">
                <ul class="navbar-nav mr-auto">
                    <% if(session.getAttribute("id") == null) { %>
                    <li class="nav-item">
                      <a class="nav-link" href="index.jsp">JSPInit</a>
                    </li>
                    <% } else { %>
                    <li class="nav-item">
                        <a class="nav-link" href="#"><% out.print(session.getAttribute("username")); %></a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="JSPCaddie.jsp">JSPCaddie</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="Login?logout=true">D&eacute;connexion</a>
                    </li>
                </ul>
                <ul class="navbar-nav navbar-right">
                    <li class="nav-item">
                        <a class="nav-link" href="JSPPay.jsp">Paiement (<% out.print(session.getAttribute("caddie")); %>)</a>
                    </li>
                    <% } %>
                </ul>
        </nav>
    </header>
                <% out.print(session.getAttribute("id") + ":" + session.getAttribute("username"));%>
    <% if(request.getParameter("error") != null) { %>
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <strong>Erreur !</strong> <% out.print(request.getParameter("error")); %>
    </div>
    <% } %>