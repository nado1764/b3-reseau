// System deps
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <fcntl.h>

#include <cerrno>
#include <cstdint>
#include <iostream>
#include <cstring>

// Project deps
#include "TCPService.hpp"

// Class definition
#include "TCPServer.hpp"

namespace tcpc {

void TCPServer::setMaxConns(uint16_t max) {
	m_maxConns = (int)max;
}

int TCPServer::getMaxConns(void) {
	return m_maxConns;
}

bool TCPServer::Listen(bool blocking) {
	if(blocking) {
		if(fcntl(m_socketFD, F_SETFL, fcntl(m_socketFD, F_GETFL, 0) | O_NONBLOCK) == -1) {
			std::cerr << "[-][tcpc] Could not set socket as non blocking : ";
			std::cerr << std::strerror(errno) << std::endl;
			return false;
		}
	}
	if(bind(m_socketFD, (struct sockaddr*)&m_socketAddress, sizeof(m_socketAddress)) == -1) {
		std::cerr << "[-][tcpc] Could not bind() : errno = ";
		std::cerr << std::strerror(errno) << std::endl;
		return false;
	}
	
	// listen
	// if m_maxConns is lower than 1, it is forced to 1
	if(listen(m_socketFD, m_maxConns < 1 ? 1 : m_maxConns) != 0) {
		std::cerr << "[-][tcpc] Could not listen() with " << m_maxConns;
		std::cerr << " as max conns : errno = " << std::strerror(errno) << std::endl;
		return false;
	}

	return true;
}

TCPService* TCPServer::Accept() {
	int ret = accept(m_socketFD, NULL, NULL);
	if(ret == -1) {
		//check errno
		if(errno == EAGAIN || errno == EWOULDBLOCK) {
			return nullptr;
		}

		throw std::strerror(errno);
	}

	return new TCPService(ret);
}

}
