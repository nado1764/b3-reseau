/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Protocoles.CIRP;

import BeanDBAccess.BeanDBAccessMySql;
import NetPackage.NetPackage;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

/**
 *
 * @author Fluxio
 */
public class CIRPServer {
    private static BeanDBAccessMySql accessBean;
    private static boolean isLoggedIn;
    
    static {
        try {
            isLoggedIn = false;
            
            //DB Access
            accessBean = new BeanDBAccessMySql();

            accessBean.Connect();
        } catch (SQLException | ClassNotFoundException ex) {
            System.out.println("Cannot reach DB : " + ex);
        } catch (Exception ex) {
            System.out.println("Error : " + ex);
        }
    }
    
    
    public static NetPackage Connect(NetPackage Package) {
        MessageDigest messageDigest;
        byte[] messageDigestMD5;
        StringBuffer stringBuffer = new StringBuffer();
        String Query = "SELECT Password " + 
                       "FROM Agents " + 
                       "WHERE Nom = '" + Package.getArg(1) + "';";
        
        ResultSet RS;
        
        try {            
            RS = accessBean.ExecuteQuery(Query); //Password retreived
            RS.next();
            
            messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(Package.getArg(3).getBytes()); //ajout du salt
            messageDigest.update(RS.getString("Password").getBytes());
            messageDigestMD5 = messageDigest.digest();
            
            
            for(byte bytes : messageDigestMD5) {
                stringBuffer.append(String.format("%02x", bytes & 0xff));
            }
            
            if(Package.getArg(2).equals(stringBuffer.toString())){
                isLoggedIn = true;
                
                Package.clearArgs();
                Package.appendArg("TRUE");
                Package = addChatInfo(Package);
                
            } else {
                Package.clearArgs();
                Package.appendArg("FALSE");
            }
            
            return Package;
            
        } catch (NoSuchAlgorithmException | SQLException e) {
            System.out.println(e);
        }
        
        Package.clearArgs();
        return Package;
    }
    
    private static NetPackage addChatInfo(NetPackage Package) {
        Properties props = loadProps();
        Package.appendArg(props.getProperty("Chat_Adress", "228.5.6.7"));
        Package.appendArg(props.getProperty("Chat_Port", "6789"));
        return Package;
    }
    
    //COMPLEMENTS
    private static Properties loadProps() {
        Properties props = new Properties();
        FileInputStream file;
        
        try {
            file = new FileInputStream("ServerChat.properties");
            props.load(file);
        } catch (IOException ex) {
            System.out.println(ex);
        }
        
        return props;
    }
}
