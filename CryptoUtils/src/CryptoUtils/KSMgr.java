/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CryptoUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;


public class KSMgr {
    private final KeyStore ks;
    private final String ksPassword;
    
    private SecretKey       sessionKey;
    private IvParameterSpec sessionIV;
    private String          sessionIVString;
    
    public KSMgr(String ksFilePath, String ksPassword) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {
        ks = KeyStore.getInstance("PKCS12");
        this.ksPassword = ksPassword;

        try (FileInputStream fis = new FileInputStream(ksFilePath)) {
            ks.load(fis, ksPassword.toCharArray());
        }
    }
    
    public KSMgr(InputStream ksFile, String ksPassword) throws KeyStoreException, IOException, NoSuchAlgorithmException, CertificateException {
        ks = KeyStore.getInstance("PKCS12");
        this.ksPassword = ksPassword;
        ks.load(ksFile, ksPassword.toCharArray());
    }
    
    public PublicKey getPublicKey(String alias) throws KeyStoreException {
        Certificate cert = ks.getCertificate(alias);
        if(cert == null) throw new NullPointerException("Alias " + alias + " non trouvé");
        return cert.getPublicKey();
    }
    
    public PrivateKey getPrivateKey(String alias, String password) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException {
        return (PrivateKey) ks.getKey(alias, password.toCharArray());
    }
    
    public SecretKey getSecretKey(String alias, String password) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException {
        return (SecretKey) ks.getKey(alias, password.toCharArray());
    }
    
    public static SecretKey generateAESKey() throws NoSuchAlgorithmException, UnsupportedEncodingException {
        System.out.println("[*] Generating AES key");
        KeyGenerator keyGen;
        keyGen = KeyGenerator.getInstance("AES");
        keyGen.init(128);
        return keyGen.generateKey();
    }
    
    public static String generateIVString() {
        return "RandomInitVector";
    }
    
    public static IvParameterSpec generateIV() throws UnsupportedEncodingException {
        System.out.println("[*] Generating IV");
        return new IvParameterSpec(generateIVString().getBytes("UTF-8"));
    }
    
    public String getIVString() {
        return new String(sessionIV.getIV());
    }
    
    public void setSessionKey(String key, String iv) throws UnsupportedEncodingException {
        System.out.println("[*] Setting key and iv for the session");
        this.setSessionKey(stringToSecretKey(key), new IvParameterSpec(iv.getBytes("UTF-8")));
    }
    
    public void setSessionKey(SecretKey key, IvParameterSpec iv) {
        sessionKey = key;
        sessionIV  = iv;
    }
    
    public SecretKey getSessionKey() {
        return sessionKey;
    }
    
    public String getSessionKeyString() {
        return Base64.getEncoder().encodeToString(sessionKey.getEncoded());
    }
    
    public SecretKey stringToSecretKey(String encodedKey) {
        byte[] decodedKey = Base64.getDecoder().decode(encodedKey);
        return new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES");
    }
    
    public IvParameterSpec getSessionIV() {
        return sessionIV;
    }
    
    public String asymEncrypt(PublicKey publicKey, String value) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");  
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);  

        byte[] encrypted = cipher.doFinal(value.getBytes());  
        return Base64.getEncoder().encodeToString(encrypted);
    }
    
    public String asymDecrypt(PrivateKey privateKey, String encrypted) throws Exception {
        Cipher cipher = Cipher.getInstance("RSA");  
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        
        byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));
        
        return new String(original);
    }
    
    public String asymSign(PrivateKey privKey, String msg) throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, SignatureException {
        Signature sig = Signature.getInstance("SHA1WithRSA");
        sig.initSign(privKey);
        sig.update(msg.getBytes("UTF-8"));
        byte[] signatureBytes = sig.sign();
        return Base64.getEncoder().encodeToString(signatureBytes);
    }
    
    public boolean asymVerifSign(PublicKey pubKey, String msg, String signature) throws NoSuchAlgorithmException, InvalidKeyException, UnsupportedEncodingException, SignatureException {
        Signature sig = Signature.getInstance("SHA1WithRSA");        
        sig.initVerify(pubKey);
        sig.update(msg.getBytes("UTF-8"));
        return sig.verify(Base64.getDecoder().decode(signature.getBytes("UTF-8")));
    }
    
    public String sessionEncrypt(String value) throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        byte[] encrypted = this.sessionEncryptByte(value);

        return Base64.getEncoder().encodeToString(encrypted);
    }
    
    public byte[] sessionEncryptByte(String value) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.ENCRYPT_MODE, sessionKey, sessionIV);

        return cipher.doFinal(value.getBytes("UTF-8"));
    }

    public String sessionDecrypt(String encrypted) throws UnsupportedEncodingException, NoSuchAlgorithmException, NoSuchPaddingException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5PADDING");
        cipher.init(Cipher.DECRYPT_MODE, sessionKey, sessionIV);

        byte[] original = cipher.doFinal(Base64.getDecoder().decode(encrypted));

        return new String(original);
    }
    
    public boolean verifySignature(String message, String macRecue, String key) throws NoSuchAlgorithmException, KeyStoreException, UnrecoverableKeyException, InvalidKeyException, UnsupportedEncodingException {
        return sign(message, key).equals(macRecue);
    }
    
    public String sign(String message, String key) throws KeyStoreException, NoSuchAlgorithmException, UnrecoverableKeyException, InvalidKeyException, UnsupportedEncodingException {
        Mac mac = Mac.getInstance("HmacSHA256");
        mac.init(new SecretKeySpec(key.getBytes(), "HmacSHA256"));
        
        return Base64.getEncoder().encodeToString(mac.doFinal(message.getBytes("UTF-8")));
    }    
    // TODO: gérer HMAC client
}