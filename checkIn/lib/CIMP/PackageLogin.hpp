#ifndef PACKAGELOGIN_HPP
#define PACKAGELOGIN_HPP

#include <string>

#include "Package.hpp"

namespace CIMP {
	class PackageLogin : public Package {
		private:
			PackageLogin();
		public:
			PackageLogin(std::string login, std::string password);
			std::tuple<std::string, std::string> getParams();
	};
}

#endif /* PACKAGELOGIN_HPP */
