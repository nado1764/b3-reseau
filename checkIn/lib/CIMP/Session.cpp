#include <algorithm>
#include <iostream>
#include <memory>
#include <string>
#include <tuple>

#include "packages.hpp"
#include "Session.hpp"
#include "String/String.h"

namespace CIMP {
  using namespace std;
  using namespace String;
  using namespace tcpc;

  //CONSTRUCTORS
  Session::Session(shared_ptr<TCPService> TServ) {
    this->TServ = TServ;
  }
  Session::Session(const Session& S) {
    this->TServ = S.getTCPServ();
  }

  //SETTERS
  void Session::setLastType(MsgType LastType) {
    this->LastType = LastType;
  }

  void Session::setTCPServ(shared_ptr<TCPService> TServ) {
    this->TServ = TServ;
  }

  //GETTERS
  MsgType Session::getLastType() const {
    return this->LastType;
  }

  shared_ptr<TCPService> Session::getTCPServ() const {
    return this->TServ;
  }

  //METHODS
  bool Session::isNextType(MsgType NextType) const {
    return isNextState(getLastType(), NextType);
  }

  bool Session::isLoggedIn() const {
    return this->IsLoggedIn;
  }

  bool Session::isLoggedIn(bool state) {
    return this->IsLoggedIn = state;
  }
  bool Session::isActive() {
	  return isAlive;
  }

  Package* Session::getMessage(bool guess) const {
    string *str = TServ->read(1000);

    if(str == nullptr)
      return nullptr;

	cout << "[*] Msg received : " << *str << endl;

    vector<string> readArgs = split(*str, '#');
    delete str;

	return Package::Create(readArgs, guess);
  }

  bool Session::sendAnswer(MsgType type, bool answer, string reason) {
	  vector<string> v = {
			  to_string(static_cast<int>(type))
			  , answer ? "Y" : "N"};

	  if(!reason.empty()) v.push_back(reason);

	  unique_ptr<Package> p(Package::Create(v, false));


	  return TServ->write(p->getMsg());
  }

  bool Session::getAnswer(MsgType type) {
	  auto p = unique_ptr<Package>(getMessage(false));

	  if(p->getType() != type || p->getArg(0) != "Y")
		  return false;

	  return true;
  }

  bool Session::sendLogin(string login, string password) {
	  PackageLogin p(login, password);

	  return TServ->write(p.getMsg());
  }

  bool Session::sendLogin(tuple<string,string> credentials) {
	  return this->sendLogin(get<0>(credentials), get<1>(credentials));
  }

  bool Session::sendLogout() {
	  PackageLogout p;
	  return TServ->write(p.getMsg());
  }

  bool Session::sendTicket(tuple<string,int> ticket) {
	  PackageCheckTicket p(get<0>(ticket), to_string(get<1>(ticket)));
	  return TServ->write(p.getMsg());
  }

  bool Session::sendLuggage(PackageLuggage &pkg) {
	  return TServ->write(pkg.getMsg());
  }

  bool Session::sendCheckLuggage() {
	  PackageCheckLuggage p;
	  return TServ->write(p.getMsg());
  }

  void Session::die() {
    this->isAlive = false;
  }
}
