/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import TICKMAP.Config;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
/**
 *
 * @author nado
 */
public class BilletsServer {
    ServerSocket tickmapSocket;
    
    // TODO: checkin ignoré ici
    BilletsServer() {     
        // Billets server
        startBilletsServer();        
    }
    
    private void startBilletsServer() {
        try {
            tickmapSocket = new ServerSocket(Config.getPortBillets(), Config.getMaxConns());
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
    
    public void listen() {
        Thread tickmapT;
        tickmapT = new Thread() {
            @Override
            public void run() {
                while(true) {
                    try {
                        System.out.println("[*][L] En attende de connexion");
                        Socket connection = tickmapSocket.accept();
                        Thread t;
                        try {
                            t = new Thread(new TICKMAPDialog(connection));
                            System.out.println("[+][L] Nouvelle connexion.");
                            t.start();
                        } catch (Exception ex) {
                            System.err.println("[-] Error: " + ex.getMessage());
                        }
                    } catch (IOException ex) {
                        System.err.println("[-] Fatal error: " + ex.getMessage());
                        System.exit(1);
                    }
                }
            }
        };
        
        tickmapT.start();
        
        while(tickmapT.isAlive()) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {}
        }
        
        if(tickmapT.isAlive()) {
            tickmapT.interrupt();
        }
        
        try {
            System.out.println("[*] Fermeture des connexions");
            tickmapSocket.close();
        } catch (IOException ex) {
            System.err.println("[-] Erreur à la fermeture des sockets");
        }
        
    }
    
    public static void main(String[] args) {
        BilletsServer Serv = new BilletsServer();
        Serv.listen();
    }
}
