#include <tuple>
#include <string>

#include "constants.hpp"
#include "PackageLogin.hpp"

namespace CIMP {

PackageLogin::PackageLogin(std::string login, std::string password) {
	setType(MsgType::LOGIN);
	Args.clear();
	Args.push_back(login);
	Args.push_back(password);
}

std::tuple<std::string, std::string> PackageLogin::getParams() {
	return std::make_tuple(Args[0], Args[1]);
}

}
