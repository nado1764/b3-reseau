/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NetPackage;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 *
 * @author nado
 */
public abstract class NetDialog implements Runnable {
    protected Socket connection;
    protected DataOutputStream DOS;
    protected BufferedReader BR;
    protected boolean Continue;
    
    public NetDialog(Socket connection) throws IOException {
        this.connection = connection;
        DOS = new DataOutputStream(connection.getOutputStream());
        BR = new BufferedReader(new InputStreamReader(connection.getInputStream()));
    }

}
