/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Client;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;

/**
 *
 * @author Fluxio
 */
public class TChatListener extends Thread {
    boolean Continue;
    
    int port;
    
    DatagramSocket DS;
    DatagramPacket DP;
    
    byte[] buffer;
    
    //CONSTRUCTOR
    public TChatListener(int port) {
        try {
            Continue = true;
            
            this.port = port;
            
            DS = new DatagramSocket();
        } catch (IOException ex) {
            
        }
    }
    
    public void run() {
        while(Continue) {
            buffer = new byte[4096];
            
            DP = new DatagramPacket(buffer, buffer.length);
            
            try {
                DS.receive(DP);
                
                
            } catch (IOException ex) {
                System.out.println(ex);
                Continue = false;
            }
        }
    }
}
