#ifndef TCPSERVICE_HPP
#define TCPSERVICE_HPP

#include <string>

#include "TCPConnection.hpp"

namespace tcpc {

class TCPService : virtual TCPConnection {
protected:
	TCPService() {}
public:
	TCPService(int socketFD);

	bool write(const char *msg, size_t len) const;
	bool write(const std::string &str) const;
	std::string* read(size_t strSize) const;
};

}

#endif
