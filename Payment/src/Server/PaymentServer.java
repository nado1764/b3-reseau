/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Server;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Fluxio
 */
public class PaymentServer {
    Properties props;
    ServerSocket paypSocket;
    
    // TODO: checkin ignoré ici
    PaymentServer() {
        try {
            loadProps();
            // Luggage server
            startLugServer(); 
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        
               
    }
    
    private void loadProps() throws FileNotFoundException, IOException {
        props = new Properties();

        FileInputStream file = new FileInputStream("server.properties");
        props.load(file);
    }
    
    private void startLugServer() throws IOException {
        paypSocket = new ServerSocket(Integer.parseInt(props.getProperty("PORT_PAY", "7004")),
                                      Integer.parseInt(props.getProperty("MAX_CONNS", "5")));
    }
    
    public void listen() {
        Thread tickmapT;
        tickmapT = new Thread() {
            @Override
            public void run() {
                while(true) {
                    try {
                        System.out.println("[*][L] En attende de connexion");
                        Socket connection = paypSocket.accept();
                        Thread t = new Thread(new PAYPDialog(connection));
                        System.out.println("[+][L] Nouvelle connexion.");
                        t.start();
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        };
        
        tickmapT.start();
        
        while(tickmapT.isAlive()) {
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {}
        }
        
        if(tickmapT.isAlive()) {
            tickmapT.interrupt();
        }
        
        try {
            System.out.println("[*] Fermeture des connexions");
            paypSocket.close();
        } catch (IOException ex) {
            System.err.println("[-] Erreur à la fermeture des sockets");
        }
        
    }
    
    public static void main(String[] args) {
        PaymentServer Serv = new PaymentServer();
        Serv.listen();
    }
}
