/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BeanDBAccess;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

/**
 *
 * @author Fluxio
 */
public class BeanDBAccessMySql extends BeanDBAccess{
    /******************************/
    /* STATIC FINAL               */
    /******************************/
    public static String DriverPropertiesFileName = "DBDriverProperties.properties";
    
    /******************************/
    /* VARIABLES                  */
    /******************************/
    public ResultSet Rs;
    
    /******************************/
    /* CONSTRUCTOR                */
    /******************************/
    public BeanDBAccessMySql() throws IOException {
        super();
        
        /******************************/
        /* VARIABLES                  */
        /******************************/
        Properties DriverProp = new Properties();

        /******************************/
        /* INIT                       */
        /******************************/
        InputStream Input = new FileInputStream(DriverPropertiesFileName);

        DriverProp.load(Input);

        this.Driver = DriverProp.getProperty("MySql");

        Input.close();
    }
    
    public BeanDBAccessMySql(String Host, String Port, String Login, String Password, String Schema) {
        super(Host, Port, Login, Password, Schema);
        
        
        this.Driver = "com.mysql.jdbc.Driver";
        
        
        /******************************/
        /* VARIABLES                  */
        /******************************/
        /*
        InputStream Input = null;
        Properties DriverProp = new Properties();
        
        try
        {
            /******************************/
            /* INIT                       */
            /******************************/
            /*
            Input = new FileInputStream(DriverPropertiesFileName);
            
            DriverProp.load(Input);
            
            this.Driver = DriverProp.getProperty("MySql");
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(BeanDBAccessMySql.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(BeanDBAccessMySql.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if(Input != null) {
                try {
                    Input.close();
                } catch (IOException ex) {
                    Logger.getLogger(BeanDBAccess.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        */
    }

    @Override
    public void Connect() throws SQLException, ClassNotFoundException {
        String Url = "jdbc:mysql://" + getHost() + ":" + getPort() + "/" + getSchema() + "?useSSL=false";
        
        Class.forName(getDriver());

        setConnection((Connection) DriverManager.getConnection(Url, getLogin(), getPassword()));

        setStatement((Statement) Connection.createStatement());
    }
    
}
