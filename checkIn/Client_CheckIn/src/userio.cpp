#include <atomic>
#include <chrono>
#include <iostream>
#include <string>
#include <thread>
#include <tuple>
#include <vector>

#include "CIMP/constants.hpp"

#include "userio.hpp"

extern volatile std::atomic<bool> sigCaught;

bool checkInput() {
	std::ios::sync_with_stdio(false);
	// FIXME: does not check for "enter" only
	bool ret = std::cin.rdbuf()->in_avail() > 0;
	std::ios::sync_with_stdio(true);

	return ret;
}

std::tuple<std::string,std::string> askCredentials() {
	// this is not thread safe
	using namespace std;

	string login, pwd;
	bool firstTime = true;

	while(login.empty()) {
		if(!firstTime)
			cout << "Login ne peut être vide" << endl;
		else
			firstTime = false;

		cout << "Login : " << flush;

		while(!checkInput()) {
			if(sigCaught) return make_tuple("", "");

			this_thread::sleep_for(std::chrono::milliseconds(10));
		}
		cin >> login;
	}

	cout << "Mot de passe : ";

	while(!checkInput()) {
		if(sigCaught) return make_tuple(login, "");

		std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}
	cin >> pwd;

	return make_tuple(login, pwd);
}

std::tuple<std::string,int> askTicket() {
	using namespace std;
	using namespace CIMP;

	cout << "\n> Numéro de billet : " << flush;

	while(!checkInput()) {
		if(sigCaught) return make_tuple("",0);
		this_thread::sleep_for(chrono::milliseconds(10));
	}
	string billet;
	cin >> billet;

	cout << "> Nombre d'accompagnants : " << flush;

	while(!checkInput()) {
		if(sigCaught) return make_tuple("",0);
		this_thread::sleep_for(chrono::milliseconds(10));
	}
	string input;
	cin >> input;

	int i;
	try {
		i = stoi(input, nullptr);
	}
	catch(std::exception& e) {
		std::cerr << e.what() << std::endl;
		i = 0;
	}
	return make_tuple(billet, i);
}

std::vector<CIMP::PackageLuggage>* askLuggages() {
	using namespace std;
	using namespace CIMP;

	int idLgg = 1;
	auto array = new vector<PackageLuggage>();

	while(true) {
		cout << "> Poids du bagage n°" << idLgg << "<Entrer une lettre si fini> : " << flush;

		while(!checkInput()) {
			if(sigCaught) {
				delete array;
				return nullptr;
			}
			this_thread::sleep_for(chrono::milliseconds(10));
		}
		string input;
		cin >> input;

		if(input.empty()) break;

		float weight;

		try {
			weight = stof(input, nullptr);
		}
		catch(std::exception& e) {
			break;
		}

		cout << "> Valise [O/N] : " << flush;

		while(!checkInput()) {
			if(sigCaught) {
				delete array;
				return nullptr;
			}
			this_thread::sleep_for(chrono::milliseconds(10));
		}
		cin >> input;

		if(input == "O" || input == "o" || input == "Y" || input == "y")
			array->push_back(PackageLuggage(weight, true));
		else
			array->push_back(PackageLuggage(weight, false));

		++idLgg;
	} // while idLgg

	return array;
}

CIMP::MsgType loggedin_menu() {
	using namespace CIMP;
	using namespace std;

	while(true) {
		string input;

		cout << "\n" << "[0] LOGOUT";
		cout << "\n" << "[1] CHECK TICKET";
		cout << "\n>>" << flush;

		while(!checkInput()) {
			if(sigCaught) return MsgType::ERROR;
			this_thread::sleep_for(chrono::milliseconds(10));
		}
		cin >> input;

		int i;
		try {
			i = stoi(input, nullptr);
		}
		catch(std::exception& e) {
			std::cerr << e.what() << std::endl;
			return MsgType::ERROR;
		}

		if(i == 0)
			return MsgType::LOGOUT;
		else if(i == 1)
			return MsgType::CHECK_TICKET;
	}
}
