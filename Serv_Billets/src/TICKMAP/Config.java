/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package TICKMAP;


import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


 
public class Config {
    private final static String defaultFilename
            = "server.properties";
    private       static Config INSTANCE = null;
    private       static Properties        prop;
    
    private Config() {
        prop = new Properties();
        loadFile(defaultFilename);       
    }
    
    private static synchronized Config getInstance() {
        if(INSTANCE == null) {
            INSTANCE = new Config();
        }
        return INSTANCE;
    }
    
    private void loadFile(String filename) {

        try {
            try (InputStream in = new FileInputStream(filename)) {
                prop.load(in);
            }
        }
        catch(FileNotFoundException e) {
            System.out.println("Fichier properties '" + filename + "' non trouvé");
        }
        catch (IOException ex) {
            System.out.println("Erreur IO : " + ex.getMessage());
        }
    }
    
    public static int getMaxConns() {
        return Integer.parseInt(getInstance().prop.getProperty("MAX_CONNS", "5"));
    }
 
    public static String getServerBillets() {
        return getInstance().prop.getProperty("HOST_BILLETS", "127.0.0.1");
    }
    
    public static String getServerPay() {
        return getInstance().prop.getProperty("HOST_PAY", "127.0.0.1");
    }
    
    public static int getPortBagages() {
        return Integer.parseInt(getInstance().prop.getProperty("PORT_BAGAGES", "7000"));
    }

    public static int getPortCheckin() {
        return Integer.parseInt(getInstance().prop.getProperty("PORT_CHECKIN", "7001"));
    }
    
    public static int getPortLugapm() {
        return Integer.parseInt(getInstance().prop.getProperty("PORT_LUGAPM", "7002"));
    }
    
    public static int getPortBillets() {
        return Integer.parseInt(getInstance().prop.getProperty("PORT_BILLETS", "7003"));
    }
    
    public static int getPortPay() {
        return Integer.parseInt(getInstance().prop.getProperty("PORT_PAY", "7004"));
    }
}